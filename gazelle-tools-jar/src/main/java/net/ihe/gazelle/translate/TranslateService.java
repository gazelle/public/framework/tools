package net.ihe.gazelle.translate;

import net.ihe.gazelle.services.GenericServiceLoader;

public class TranslateService {

	private TranslateService() {
		super();
	}

	private static TranslateProvider getProvider() {
		return GenericServiceLoader.getService(TranslateProvider.class);
	}

	public static String getTranslation(String key) {
		return getProvider().getTranslation(key);
	}
}
