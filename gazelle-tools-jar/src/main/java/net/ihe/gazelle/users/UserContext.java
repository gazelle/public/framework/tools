package net.ihe.gazelle.users;

public interface UserContext {


    /**
     * Retrieve selected testing session id from cookie.
     *
     * @return the selected testing session id from cookie if it exists, otherwise return -1
     */
    Integer getSelectedTestingSessionId();

    /**
     * Sets selected testing session id in a session cookie.
     *
     * @param testingSessionId the selected testing session id
     */
    void setSelectedTestingSessionId(Integer testingSessionId);

}
