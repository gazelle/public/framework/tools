/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.util;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Iterator;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 */
public class ThumbnailCreator {

    public static final int HEIGHT = 150;
    public static final float QUALITY = 0.95f;

    public static void createThumbnail(String filename, int thumbWidth, int thumbHeight, int quality, String outFilename)
            throws InterruptedException, FileNotFoundException, IOException {
        Image image = Toolkit.getDefaultToolkit().getImage(filename);
        createThumbnail(image, thumbWidth, thumbHeight, quality, outFilename);
    }

    public static void createThumbnail(Image image, int thumbWidth, int thumbHeight, int quality, String outFilename)
            throws InterruptedException, FileNotFoundException, IOException {
        BufferedImage thumbImage = createThumbnail(image, thumbWidth, thumbHeight);
        writeImage(thumbImage, new File(outFilename));
    }

    public static BufferedImage createThumbnail(Image image, int thumbWidth, int thumbHeight)
            throws InterruptedException {
        MediaTracker mediaTracker = new MediaTracker(new Container());
        mediaTracker.addImage(image, 0);
        mediaTracker.waitForID(0);

        double thumbRatio = (double) thumbWidth / (double) thumbHeight;
        int imageWidth = image.getWidth(null);
        int imageHeight = image.getHeight(null);
        double imageRatio = (double) imageWidth / (double) imageHeight;
        if (thumbRatio < imageRatio) {
            thumbHeight = (int) (thumbWidth / imageRatio);
        } else {
            thumbWidth = (int) (thumbHeight * imageRatio);
        }

        BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = thumbImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
        return thumbImage;
    }

    public static byte[] createThumbnailPNG(byte[] data) throws IOException {
        return createThumbnailPNG(data, HEIGHT);
    }

    public static byte[] createThumbnailPNG(InputStream is) throws IOException {
        return createThumbnailPNG(is, HEIGHT);
    }

    public static byte[] createThumbnailPNG(byte[] data, int newHeight) throws IOException {
        return createThumbnailPNG(new ByteArrayInputStream(data), newHeight);
    }

    public static byte[] createThumbnailPNG(InputStream is, int newHeight) throws IOException {
        BufferedImage original = ImageIO.read(is);
        int originalHeight = original.getHeight();
        BufferedImage resizedImage = null;
        if (originalHeight > newHeight) {
            int originalWidth = original.getWidth();
            int newWidth = (originalWidth * newHeight) / originalHeight;
            resizedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = resizedImage.createGraphics();
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g.drawImage(original, 0, 0, newWidth, newHeight, null);
            g.dispose();
        } else {
            resizedImage = original;
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(resizedImage, "png", bos);
        bos.close();
        return bos.toByteArray();
    }

    private static void writeImage(BufferedImage image, File imageFile) throws IOException {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);

            ImageWriter writer = null;
            Iterator<?> iter = ImageIO.getImageWritersByFormatName("JPEG");
            if (!iter.hasNext()) {
                throw new IOException("No Writers Available");
            }
            writer = (ImageWriter) iter.next();
            ImageOutputStream ios = ImageIO.createImageOutputStream(fos);
            writer.setOutput(ios);
            JPEGImageWriteParam iwp = new JPEGImageWriteParam(null);
            iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            iwp.setCompressionQuality(QUALITY);
            writer.write(null, new IIOImage(image, null, null), iwp);
            ios.flush();
            writer.dispose();
            ios.close();
        }finally {
            if (fos != null)
                fos.close();
        }
    }

    public static String createNewFileName(String filename) {
        int indexOfSeparator = filename.lastIndexOf(java.io.File.separatorChar);
        String pathname = filename.substring(0, indexOfSeparator);
        String name = filename.substring(indexOfSeparator + 1);
        return pathname + java.io.File.separatorChar + "thumbnail_" + name;
    }
}
