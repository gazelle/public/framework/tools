package net.ihe.gazelle.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cel on 24/02/16.
 */
public class GazellePosixFilePermissions {

    private static Logger log = LoggerFactory.getLogger(GazellePosixFilePermissions.class);

    private static final String JBOSS_GROUP = "jboss-admin";
    private static GroupPrincipal jBossGroupPrincipal ;

    private static final Set<PosixFilePermission> JBOSS_PERMISSIONS = new HashSet<PosixFilePermission>(
            Arrays.asList(PosixFilePermission.OWNER_READ,
                    PosixFilePermission.OWNER_WRITE,
                    PosixFilePermission.OWNER_EXECUTE,
                    PosixFilePermission.GROUP_READ,
                    PosixFilePermission.GROUP_WRITE,
                    PosixFilePermission.GROUP_EXECUTE,
                    PosixFilePermission.OTHERS_READ,
                    PosixFilePermission.OTHERS_EXECUTE));


    /**
     * Set POSIX permissions on a file or a directory to "rwxrwxr-x" with group "jboss-admin"
     * @param path
     */
    public static void setJBossPermissions(String path) {
        if (path != null) {
            PosixFileAttributeView fileAttribute = Files.getFileAttributeView(Paths.get(path),
                    PosixFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
            setJBossPermissions(path, fileAttribute);
        }
    }

    private static void setJBossPermissions(String path, PosixFileAttributeView fileAttribute) {
        try {
            fileAttribute.setPermissions(JBOSS_PERMISSIONS);
            fileAttribute.setGroup(getJBossGroupPrincipal());
        } catch (IOException e) {
            log.warn("Unexpected error while setting attributes on the file/directory '" + path + "' : " + e.getMessage());
        } catch (ClassCastException e) {
            log.warn("the file/directory '" + path + "' does not support Posix File Attributes : " + e.getMessage());
        } catch (UnsupportedOperationException e) {
            log.warn("The file system does not support Posix File Attributes : " + e.getMessage());
        } catch (SecurityException e) {
            log.warn("Access denied to set permissions to file/directory '" + path + "' : " + e.getMessage());
        }
    }

    private static GroupPrincipal getJBossGroupPrincipal() {
        if (jBossGroupPrincipal == null) {
            UserPrincipalLookupService lookupService = FileSystems.getDefault().getUserPrincipalLookupService();
            try  {
                jBossGroupPrincipal = lookupService.lookupPrincipalByGroupName(JBOSS_GROUP);
            } catch (UserPrincipalNotFoundException e) {
                log.warn("The principal '" + JBOSS_GROUP + "' does not exists or is not a group : " + e.getMessage());
            } catch (IOException e) {
                log.warn(e.getMessage());
            } catch (SecurityException e) {
                log.warn(e.getMessage());
            }
        }
        return jBossGroupPrincipal;
    }
}
