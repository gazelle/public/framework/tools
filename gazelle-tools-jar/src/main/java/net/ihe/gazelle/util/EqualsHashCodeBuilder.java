package net.ihe.gazelle.util;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Id;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Automatic computing of hashCode and equals using Hibernate annotations
 *
 * @author glandais
 */
public class EqualsHashCodeBuilder {

    private static final Map<Class<?>, List<String>> IDS_MAP = Collections
            .synchronizedMap(new HashMap<Class<?>, List<String>>());

    /**
     * Retrieves fields used for computing hashCode/equals
     * <p>
     * Users fields with @Id and/or @Column(unique=true)
     *
     * @param clazz
     *
     * @return
     */
    private static List<String> computeIds(Class<?> clazz) {
        List<String> result = new ArrayList<String>();

        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            Id id = field.getAnnotation(Id.class);
            if (id != null) {
                result.add(field.getName());
            } else {
                Column column = field.getAnnotation(Column.class);
                if (column != null && column.unique()) {
                    result.add(field.getName());
                }
            }
        }

        Class<?> superclass = clazz.getSuperclass();
        if (superclass != null) {
            List<String> parentIds = computeIds(superclass);
            result.addAll(parentIds);
        }

        return result;
    }

    public static final boolean equals(Class<?> clazz, Object o1, Object o2) {
        if (o1 == o2) {
            return true;
        }
        if (o1 == null && o2 != null) {
            return false;
        }
        if (o1 != null && o2 == null) {
            return false;
        }
        if (!clazz.isAssignableFrom(o1.getClass())) {
            return false;
        }
        if (!clazz.isAssignableFrom(o2.getClass())) {
            return false;
        }
        List<String> ids = getIds(clazz);
        EqualsBuilder equalsBuilder = new EqualsBuilder();
        for (String id : ids) {
            Object id1 = getValue(o1, id);
            Object id2 = getValue(o2, id);
            equalsBuilder.append(id1, id2);
            if (!equalsBuilder.isEquals()) {
                return false;
            }
        }
        return equalsBuilder.isEquals();
    }

    private static List<String> getIds(Class<?> clazz) {
        List<String> ids = IDS_MAP.get(clazz);
        if (ids == null) {
            synchronized (IDS_MAP) {
                ids = IDS_MAP.get(clazz);
                if (ids == null) {
                    ids = computeIds(clazz);
                    IDS_MAP.put(clazz, ids);
                }
            }
        }
        return ids;
    }

    private static Object getValue(Object o, String id) {
        Object idValue;
        try {
            idValue = PropertyUtils.getProperty(o, id);
        } catch (Throwable e) {
            return null;
        }
        return idValue;
    }

    public static final int hashCode(Class<?> clazz, Object o) {
        if (o == null) {
            return 0;
        }
        List<String> ids = getIds(clazz);
        HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
        for (String id : ids) {
            Object idValue = getValue(o, id);
            hashCodeBuilder.append(idValue);
        }
        return hashCodeBuilder.toHashCode();
    }

    private EqualsHashCodeBuilder() {
        super();
    }
}
