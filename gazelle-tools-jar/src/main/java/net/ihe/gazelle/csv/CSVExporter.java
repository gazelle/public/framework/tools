package net.ihe.gazelle.csv;

import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

public class CSVExporter {

	private CSVExporter() {
		super();
	}

	public static String exportCSV(List<CSVExportable> exportables) {
		StringBuffer toReturn = new StringBuffer();
		if (exportables != null && exportables.size() > 0) {
			List<String> csvHeaders = exportables.get(0).getCSVHeaders();
			addLine(toReturn, csvHeaders);
			for (CSVExportable exportable : exportables) {
				List<String> csvValues = exportable.getCSVValues();
				addLine(toReturn, csvValues);
			}
		}
		return toReturn.toString();
	}

	private static void addLine(StringBuffer toReturn, List<String> csvColumns) {
		boolean first = true;
		for (String string : csvColumns) {
			if (first) {
				first = false;
			} else {
				toReturn.append(",");
			}
			toReturn.append(StringEscapeUtils.escapeCsv(string));
		}
		toReturn.append("\r\n");
	}

}
