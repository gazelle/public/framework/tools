UPDATE test_application_preference SET value = '${fqdn}' WHERE name = 'fqdn';
UPDATE test_application_preference SET value = '${sso.enabled}' WHERE name = 'sso_enabled';
UPDATE test_application_preference SET value = '${OVERRIDDEN_URL_ENV}' WHERE name = 'application_url';