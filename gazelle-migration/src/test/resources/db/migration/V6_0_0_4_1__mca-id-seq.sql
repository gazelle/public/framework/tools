ALTER SEQUENCE mca_doc_type_seq RENAME TO mca_doc_type_id_seq ;
ALTER SEQUENCE mca_content_analysis_config_seq RENAME TO mca_content_analysis_config_id_seq ;
ALTER SEQUENCE mca_mime_type_config_seq RENAME TO mca_mime_type_config_id_seq ;
ALTER SEQUENCE mca_xml_tag_config_seq RENAME TO mca_xml_tag_config_id_seq ;
