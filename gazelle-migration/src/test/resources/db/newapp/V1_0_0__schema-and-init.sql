
--
-- Name: cmn_application_preference; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cmn_application_preference (
                                                   id integer NOT NULL,
                                                   class_name character varying(255) NOT NULL,
                                                   description character varying(255) NOT NULL,
                                                   preference_name character varying(64) NOT NULL,
                                                   preference_value character varying(512) NOT NULL
);

--
-- Name: cmn_application_preference_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cmn_application_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: cmn_application_preference cmn_application_preference_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cmn_application_preference
    ADD CONSTRAINT cmn_application_preference_pkey PRIMARY KEY (id);



--
-- Name: mca_doc_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mca_doc_type (
                                     id integer NOT NULL,
                                     name character varying(255)
);


--
-- Name: mca_doc_type_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mca_doc_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mca_content_analysis_config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mca_content_analysis_config (
                                                    id integer NOT NULL,
                                                    doc_type character varying(255),
                                                    validation_type character varying(255),
                                                    byte_pattern bytea,
                                                    sample text,
                                                    string_pattern character varying(255),
                                                    unwanted_content character varying(255)
);


--
-- Name: mca_content_analysis_config_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mca_content_analysis_config_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mca_mime_type_config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mca_mime_type_config (
                                             id integer NOT NULL,
                                             doc_type character varying(255),
                                             validation_type character varying(255),
                                             mime_type character varying(255),
                                             sample text
);

--
-- Name: mime_type_config_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mca_mime_type_config_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: mca_xml_tag_config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mca_xml_tag_config (
                                           id integer NOT NULL,
                                           doc_type character varying(255),
                                           validation_type character varying(255),
                                           namespace character varying(255),
                                           sample text,
                                           tag_name character varying(255)
);


--
-- Name: xml_tag_config_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mca_xml_tag_config_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;





--
-- INITIALIZATION
--

INSERT INTO cmn_application_preference (id, preference_name, preference_value, class_name, description)
VALUES (nextval('cmn_application_preference_id_seq'), 'application_url', 'http://localhost/EVSClient/', 'java.lang.String', 'URL to reach the tool');