--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5 (Ubuntu 13.5-0ubuntu0.21.04.1)
-- Dumped by pg_dump version 13.5 (Ubuntu 13.5-0ubuntu0.21.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';


--
-- Name: cmn_application_preference; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cmn_application_preference (
                                                   id integer NOT NULL,
                                                   class_name character varying(255) NOT NULL,
                                                   description character varying(255) NOT NULL,
                                                   preference_name character varying(64) NOT NULL,
                                                   preference_value character varying(512) NOT NULL
);

--
-- Name: cmn_application_preference_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cmn_application_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: cmn_application_preference cmn_application_preference_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cmn_application_preference
    ADD CONSTRAINT cmn_application_preference_pkey PRIMARY KEY (not_existing_column);



