package net.ihe.gazelle.migration;

import net.ihe.gazelle.migration.application.MigrationException;
import net.ihe.gazelle.model4test.ApplicationPreference;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PersistenceXmlConfigTest {

   private EntityManagerFactory entityManagerFactory;
   private EntityManager entityManager;

   @Before
   public void setUp() throws SQLException {
      JdbcTestServer.resetDatabase(JdbcTestServer.TEST_DATABASE);
   }

   @After
   public void tearDown() {
      if (entityManager != null) {
         entityManager.close();
      }
      if (entityManagerFactory != null) {
         entityManagerFactory.close();
      }
   }

   @Test
   public void testH2InMemory() {
      testSuccessfullConfig("h2-in-memory-test");
   }

   @Test
   public void testJDBCPostgreSQL() {
      testSuccessfullConfig("jdbc-postgres-test");
   }

   @Test(expected = MigrationException.class)
   public void testNoDataSourceError() {
      entityManagerFactory = Persistence.createEntityManagerFactory(
            "no-datasource-test");
   }

   @Test
   public void testPlaceholderPath() {
      entityManagerFactory = Persistence.createEntityManagerFactory("placeholders-path-test");
      entityManager = entityManagerFactory.createEntityManager();

      assertNotNull(entityManager);
      assertEquals("pcdev.localhost", entityManager.find(ApplicationPreference.class, "fqdn").getValue());
   }

   @Test
   public void testPlaceholderLoadingPriority() {
      entityManagerFactory = Persistence.createEntityManagerFactory("placeholders-path-test");
      entityManager = entityManagerFactory.createEntityManager();
      assertNotNull(entityManager);
      //default value: http://original, modified by environment variable OVERRIDDEN_URL_ENV
      assertEquals("http://modified", entityManager.find(ApplicationPreference.class, "application_url").getValue());
   }

   @Test(expected = MigrationException.class)
   public void testPlaceholderPathError() {
      entityManagerFactory = Persistence.createEntityManagerFactory("placeholders-path-error-test");
   }

   private void testSuccessfullConfig(String persistenceUnitName) {
      entityManagerFactory = Persistence.createEntityManagerFactory(
            persistenceUnitName);
      entityManager = entityManagerFactory.createEntityManager();

      assertNotNull(entityManager);
      assertDatabaseInitialized(entityManager);
   }

   private void assertDatabaseInitialized(EntityManager entityManager) {
      assertEquals("localhost", entityManager.find(ApplicationPreference.class, "fqdn").getValue());
   }

}
