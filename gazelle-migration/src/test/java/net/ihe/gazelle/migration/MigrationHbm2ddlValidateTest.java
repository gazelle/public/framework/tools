package net.ihe.gazelle.migration;

import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;

/**
 * Verify Migration module can run with hbm2ddl validation.
 */
public class MigrationHbm2ddlValidateTest {

   @Before
   public void setUp() throws SQLException {
      JdbcTestServer.resetDatabase(JdbcTestServer.TEST_DATABASE);
   }

   /**
    * Verify that hbm2ddl-schema-validation is triggered AFTER migrations.
    */
   @Test
   public void testMigrationWithHbm2ddlValidation() {
      EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(
            "validate-test");
      assertNotNull(entityManagerFactory.createEntityManager());
      entityManagerFactory.close();
   }

   /**
    * Verify that errors detected by hbm2ddl-schema-validation prevent EntityManagers to be created and raise an
    * exception to stop deployment.
    * <p>
    * The test is using another database, because Hibernate org.hibernate.internal.SessionFactoryImpl is failing to
    * close the db connection when schema-validation throws errors, and thus prevent other unit-tests to reset the DB
    * and run. This is a potential resource leak on Hibernate 4.2.0 if JVM is not stopped.
    */
   @Test(expected = PersistenceException.class)
   public void testMigrationWithHbm2ddlValidationError() throws SQLException {
      JdbcTestServer.resetDatabase("migration-test-for-unclosed-connection");
      EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(
            "validate-error-test");
   }

}
