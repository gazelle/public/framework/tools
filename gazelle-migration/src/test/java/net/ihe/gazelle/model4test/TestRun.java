package net.ihe.gazelle.model4test;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Fake entity for migration tests
 */
@Entity
@Table(name = "test_test_run", schema = "public")
@SequenceGenerator(name = "TestRunSequence", sequenceName = "test_test_run_id_seq", allocationSize = 1)
public class TestRun {

   public static enum TestResult {
      PASSED,
      FAILED,
      ERROR,
      SKIPPED;
   }

   @Id
   @Column(name = "id")
   @GeneratedValue(generator = "TestRunSequence", strategy = GenerationType.SEQUENCE)
   private Integer id;

   @Column(name = "test_id", length = 127, nullable = false)
   private String testId;

   @Temporal(TemporalType.TIMESTAMP)
   @Column(name = "date")
   private Date date;

   @Enumerated(EnumType.STRING)
   @Column(name = "result", length = 31)
   private TestResult result;

   @OneToMany(mappedBy = "testRun", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
   private List<TestStepRun> stepRuns;

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getTestId() {
      return testId;
   }

   public void setTestId(String testId) {
      this.testId = testId;
   }

   public Date getDate() {
      return date;
   }

   public void setDate(Date date) {
      this.date = date;
   }

   public TestResult getResult() {
      return result;
   }

   public void setResult(TestResult result) {
      this.result = result;
   }

   public List<TestStepRun> getStepRuns() {
      return stepRuns;
   }

   public void setStepRuns(List<TestStepRun> stepRuns) {
      this.stepRuns = stepRuns;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof TestRun)) {
         return false;
      }
      TestRun testRun = (TestRun) o;
      return Objects.equals(testId, testRun.testId) && Objects.equals(date,
            testRun.date) && result == testRun.result && Objects.equals(stepRuns, testRun.stepRuns);
   }

   @Override
   public int hashCode() {
      return Objects.hash(testId, date, result, stepRuns);
   }
}
