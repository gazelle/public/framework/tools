package net.ihe.gazelle.migration.interlay.adapter;

import net.ihe.gazelle.migration.application.MigrationException;
import net.ihe.gazelle.migration.application.MigrationService;
import net.ihe.gazelle.migration.application.Migrator;
import org.apache.commons.io.IOUtils;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.flywaydb.core.api.MigrationInfoService;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.FlywaySqlScriptException;
import org.flywaydb.core.internal.dbsupport.SqlScript;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

/**
 * Migrator adapter for FlywayDB over a PostgreSQL database or an H2 database (test only).
 */
public class FlywayMigratorAdapter implements Migrator {


   private final DbSpecifics dbSpecifics;
   private final DataSource dataSource;
   private final Flyway flyway;

   public FlywayMigratorAdapter(DataSource dataSource, String[] migrationPaths, Map<String, String> placeholders) {
      this.dataSource = dataSource;
      this.dbSpecifics = buildDbSpecifics(dataSource);
      flyway = new Flyway();
      flyway.setDataSource(dataSource);
      if (migrationPaths != null && migrationPaths.length > 0) {
         flyway.setLocations(migrationPaths);
      }
      if(placeholders != null) {
         flyway.setPlaceholders(placeholders);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean isDatabaseManaged() {
      try {
         MigrationInfoService infoService = flyway.info();
         return infoService.current() != null;
      } catch (FlywayException e) {
         return false;
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean isSchemaCreated() {
      try (Connection connection = dataSource.getConnection()) {
         return dbSpecifics.isSchemaCreated(connection);
      } catch (SQLException e) {
         throw new MigrationException("Error while checking the existance of the schema.", e);
      }
   }


   /**
    * {@inheritDoc}
    */
   @Override
   public void createSchemaAndInit(final String baselinePath) {
      try (Connection connection = dataSource.getConnection()) {
         DbSupport dbSupport = dbSpecifics.getDbSupport(connection);
         runSqlScript(dbSupport, baselinePath + MigrationService.BASELINE_SCHEMA_SQL);
         setDefaultPublicSchema(connection);
         runSqlScript(dbSupport, baselinePath + MigrationService.BASELINE_INIT_SQL);
      } catch (SQLException | FlywaySqlScriptException e) {
         throw new MigrationException("Unable to create schema and init data", e);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setBaseline(String version) {
      flyway.setBaselineVersionAsString(version);
      flyway.setBaselineDescription("Baseline");
      flyway.setBaselineOnMigrate(true);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void migrate() {
      flyway.migrate();
   }

   private DbSpecifics buildDbSpecifics(DataSource dataSource) {
      return dataSource.getClass().getCanonicalName().startsWith("org.h2") ?
            new H2Specifics() :
            new PostgreSQLSpecifics();
   }

   private void runSqlScript(DbSupport dbSupport, String scriptResourcePath) {
      SqlScript sqlScript = new SqlScript(getScriptContent(scriptResourcePath), dbSupport);
      sqlScript.execute(dbSupport.getJdbcTemplate());
   }

   private String getScriptContent(String resourcePath) {
      try (InputStream inputStream = this.getClass().getResourceAsStream(resourcePath)) {
         return new String(IOUtils.toByteArray(inputStream), StandardCharsets.UTF_8);
      } catch (IOException e) {
         throw new MigrationException(String.format("Unable to read script content %s", resourcePath), e);
      }
   }

   private void setDefaultPublicSchema(Connection connection) throws SQLException {
      try (Statement statement = connection.createStatement()) {
         statement.execute("SET search_path = public ;");
      }
   }
}
