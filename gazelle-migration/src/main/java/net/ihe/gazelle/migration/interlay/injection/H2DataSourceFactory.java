package net.ihe.gazelle.migration.interlay.injection;

import org.h2.jdbcx.JdbcDataSource;

import javax.sql.DataSource;

class H2DataSourceFactory extends AbstractDataSourceFactory{

   @Override
   DataSource getDataSource(String url, String user, String password) {
      JdbcDataSource dataSource = new JdbcDataSource();
      dataSource.setURL(url);
      dataSource.setUser(user);
      dataSource.setPassword(password);
      return dataSource;
   }
}
