package net.ihe.gazelle.migration.application;

/**
 * Migrator executor. Interface used to abstract the underlying tool.
 */
public interface Migrator {

   /**
    * Is the connected database already managed (in version migration) by the tool?
    *
    * @return true if the databsae is already managed by the tool, false otherwise.
    */
   boolean isDatabaseManaged();

   /**
    * Is there already an applicative schema in the connected database?
    *
    * @return true if applicative schema are present in the databse, false otherwise.
    */
   boolean isSchemaCreated();

   /**
    * Create DDL Schema and initialize data, using the given baseline resource path. The baseline resource path must
    * contain two files : schema.sql and init.sql.
    *
    * @param baselinePath resource folder path.
    */
   void createSchemaAndInit(final String baselinePath);

   /**
    * Define the version tag to use to identify the baseline.
    *
    * @param version version tag.
    */
   void setBaseline(String version);

   /**
    * Verify and run available migrations. If setBaseline has been called, it will first tag the baseline before running
    * migrations.
    */
   void migrate();

}

