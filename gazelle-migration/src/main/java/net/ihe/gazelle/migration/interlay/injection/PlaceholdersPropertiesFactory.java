package net.ihe.gazelle.migration.interlay.injection;

import net.ihe.gazelle.migration.application.MigrationException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PlaceholdersPropertiesFactory {

   private final Map<String, String> placeholders ;

   public PlaceholdersPropertiesFactory(String placeholdersPath) {
      placeholders = asMap(
            loadProperties(placeholdersPath)
      );
   }

   public Map<String, String> getPlaceholders() {
      return placeholders;
   }

   private Map<String, String> asMap(Properties properties) {
      Map<String, String> placeholdersMap = new HashMap<>();
      for (String placeholderName : properties.stringPropertyNames()) {
         placeholdersMap.put(placeholderName, properties.getProperty(placeholderName));
      }
      return placeholdersMap;
   }

   private Properties loadProperties(String propertiesPath) {
      try (InputStream is = new FileInputStream(propertiesPath)) {
         Properties properties = new Properties();
         properties.load(is);
         return properties;
      } catch (IOException e) {
         throw new MigrationException(
               String.format("Unable to load placeholders properties %s.", propertiesPath), e);
      }
   }
}
