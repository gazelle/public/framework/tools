package net.ihe.gazelle.migration.interlay.adapter;

import org.flywaydb.core.internal.dbsupport.DbSupport;

import java.sql.Connection;
import java.sql.SQLException;

interface DbSpecifics {

   boolean isSchemaCreated(Connection connection) throws SQLException;

   DbSupport getDbSupport(Connection connection);

}
