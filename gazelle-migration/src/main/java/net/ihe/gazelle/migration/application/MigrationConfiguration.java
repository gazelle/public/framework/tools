package net.ihe.gazelle.migration.application;

import javax.sql.DataSource;
import java.util.Map;

/**
 * Configuration provider for {@link MigrationService}
 */
public interface MigrationConfiguration {

   public static final String DEFAULT_MIGRATION_CLASSPATH = "/db/migration";
   public static final String DEFAULT_BASELINE_CLASSPATH = "/db/baseline";

   /**
    * Is automated migration enabled or not.
    *
    * @return true if enabled, false otherwise
    */
   boolean isMigrationEnabled();

   /**
    * Get a valid {@link DataSource} to connect to the database to migrate.
    *
    * @return the datasource.
    */
   DataSource getDataSource();

   /**
    * Get configured migrations resource paths (folder in the classpath). Several folders may be provided.
    *
    * @return the list of configured resource folder paths, or null if not configured.
    */
   String[] getMigrationClassPaths();

   /**
    * Get the configured baseline resource path (folder in the classpath).
    *
    * @return the configured resource folder path, or null if not configured.
    */
   String getBaselineClassPath();

   /**
    * Get a key-value map of placeholders to replace in migration scripts
    *
    * @return a key-value map. May be null.
    */
   Map<String, String> getPlaceholders();

}
