package net.ihe.gazelle.migration.application;

import net.ihe.gazelle.migration.interlay.adapter.FlywayMigratorAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.Arrays;

/**
 * Migration service. Verify provided {@link MigrationConfiguration} and run data-migrations. If using the service on an
 * unmanaged and existing database, the service will defined the existing database data and schema as baseline, and then
 * apply migrations on it.
 */
public class MigrationService {

   public static final String BASELINE_SCHEMA_SQL = "/schema.sql";
   public static final String BASELINE_INIT_SQL = "/init.sql";
   private static final String BASELINE_VERSION = "0_0_0";

   private static final Logger LOG = LoggerFactory.getLogger(MigrationService.class);

   private final MigrationConfiguration migrationConfiguration;

   /**
    * Instantiate the migration service.
    *
    * @param migrationConfiguration configuration of the service.
    *
    * @throws IllegalArgumentException if the configuration is null.
    * @see MigrationConfiguration
    */
   public MigrationService(MigrationConfiguration migrationConfiguration) {
      if (migrationConfiguration != null) {
         this.migrationConfiguration = migrationConfiguration;
      } else {
         throw new IllegalArgumentException("MigrationConfiguration must be defined");
      }
   }

   /**
    * Run migration service.
    *
    * @throws MigrationException                    in case of configuration error, connection error or baseline error.
    * @throws org.flywaydb.core.api.FlywayException in case of migration error.
    */
   public void run() {
      if (migrationConfiguration.isMigrationEnabled()) {
         LOG.debug("Data migration enabled");
         String[] migrationClassPaths = getMigrationPathsOrDefault(migrationConfiguration);
         String baselineClassPath = getBaselinePathOrDefault(migrationConfiguration);
         if (isBaselineProvided(baselineClassPath) || areMigrationsProvided(migrationClassPaths)) {
            LOG.debug("Valid baseline or migration classpath configuration.");
            doMigration(migrationClassPaths, baselineClassPath);
         } else {
            throw new MigrationException(
                  "Both migrations and baseline classpath configured does not exists or are empty. " +
                        "Please verify the configuration.");
         }
      } else {
         LOG.debug("Data migration disabled");
      }
   }

   private void doMigration(String[] migrationClassPaths, String baselineClassPath) {
      LOG.info("Starting migration service...");
      Migrator migrator = new FlywayMigratorAdapter(
            migrationConfiguration.getDataSource(),
            migrationClassPaths,
            migrationConfiguration.getPlaceholders()
      );

      if (isBaselineProvided(baselineClassPath) && !migrator.isDatabaseManaged()) {
         LOG.warn("Unmanaged database detected. Versions tracking will be created by Migration service.");
         if (!migrator.isSchemaCreated()) {
            LOG.warn("Creation of the schema and data initialization.");
            migrator.createSchemaAndInit(baselineClassPath);
         }
         migrator.setBaseline(BASELINE_VERSION);
      }
      LOG.info("Migration service ready to migrate.");
      migrator.migrate();
      LOG.info("Migration service completed.");
   }

   private boolean isBaselineProvided(String baselineClassPath) {
      return resourceExists(baselineClassPath + BASELINE_SCHEMA_SQL) &&
            resourceExists(baselineClassPath + BASELINE_INIT_SQL);
   }

   private boolean areMigrationsProvided(String[] migrationClassPaths) {
      for (String migrationClassPath : migrationClassPaths) {
         URL migrationFolder = this.getClass().getResource(migrationClassPath);
         if (migrationFolder != null && hasAtLeastOneMigration(migrationFolder)) {
            return true;
         }
      }
      return false;
   }

   private boolean hasAtLeastOneMigration(URL migrationFolderURL) {
      File file = new File(migrationFolderURL.getPath());
      return file.listFiles().length > 0;
   }

   private String getBaselinePathOrDefault(MigrationConfiguration migrationConfiguration) {
      String path = migrationConfiguration.getBaselineClassPath();
      if (path != null && !path.isEmpty()) {
         LOG.debug("Baseline classpath = {}", path);
         return path;
      } else {
         LOG.debug("Use default baseline classpath = {}", MigrationConfiguration.DEFAULT_BASELINE_CLASSPATH);
         return MigrationConfiguration.DEFAULT_BASELINE_CLASSPATH;
      }
   }

   private String[] getMigrationPathsOrDefault(MigrationConfiguration migrationConfiguration) {
      String[] paths = migrationConfiguration.getMigrationClassPaths();
      if (paths != null && paths.length > 0) {
         LOG.debug("Migration classpaths = {}", Arrays.asList(paths));
         return paths;
      } else {
         LOG.debug("Use default migration classpaths = {}", MigrationConfiguration.DEFAULT_MIGRATION_CLASSPATH);
         return new String[]{MigrationConfiguration.DEFAULT_MIGRATION_CLASSPATH};
      }
   }

   private boolean resourceExists(String resourcePath) {
      return this.getClass().getResource(resourcePath) != null;
   }

}
