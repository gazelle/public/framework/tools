package net.ihe.gazelle.migration.interlay.adapter;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.postgresql.PostgreSQLDbSupport;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

class PostgreSQLSpecifics implements DbSpecifics {

   /**
    * SQL Query to test if database is empty (aka created with creatdb or CREATE DATABASE, without any schema
    * initialization nor data). PostrgreSQL specific.
    *
    * @see https://stackoverflow.com/a/42693458/2725126
    **/
   private static final String IS_PSQL_DATABASE_EMPTY = "SELECT count(*) FROM pg_class c JOIN pg_namespace s on s.oid" +
         " = c.relnamespace WHERE s.nspname NOT IN ('pg_catalog', 'pg_toast', 'information_schema') AND s.nspname NOT" +
         " LIKE 'pg_temp%';";

   @Override
   public DbSupport getDbSupport(Connection connection) {
      return new PostgreSQLDbSupport(connection);
   }

   @Override
   public boolean isSchemaCreated(Connection connection) throws SQLException {
      try (Statement statement = connection.createStatement()) {
         return isSchemaCreated(statement);
      }
   }

   private boolean isSchemaCreated(Statement statement) throws SQLException {
      try(ResultSet resultSet = statement.executeQuery(IS_PSQL_DATABASE_EMPTY)) {
         resultSet.next();
         return resultSet.getInt("count") != 0;
      }
   }
}
