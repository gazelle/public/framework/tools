package net.ihe.gazelle.migration.interlay.injection;

import net.ihe.gazelle.migration.application.MigrationConfiguration;

import java.util.HashMap;
import java.util.Map;

/**
 * Abstract MigrationConfiguration, that at least load placeholders from env. variables.
 */
public abstract class AbstractEnvMigrationConfiguration implements MigrationConfiguration {

   @Override
   public Map<String, String> getPlaceholders() {
      return new HashMap<>(System.getenv());
   }

}
