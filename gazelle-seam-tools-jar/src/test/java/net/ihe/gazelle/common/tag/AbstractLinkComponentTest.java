package net.ihe.gazelle.common.tag;

import net.ihe.gazelle.common.LinkDataProvider;
import org.junit.Test;

import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;
import java.util.List;

public class AbstractLinkComponentTest {

    @Test
    public void urlTest() {

        String expected = "http://localhost/project/any/link";

        LinkComponentTest lct = new LinkComponentTest();
        String url1 = lct.getURL(new LinkDataProviderTest(), "with-slash", "http://localhost/project");
        String url2 = lct.getURL(new LinkDataProviderTest(), "without-slash", "http://localhost/project/");
        String url3 = lct.getURL(new LinkDataProviderTest(), "without-slash", "http://localhost/project");

        assert url1.equals(expected);
        assert url2.equals(expected);
        assert url3.equals(expected);

    }

    //Stub implementation for test
    class LinkComponentTest extends AbstractLinkComponent {

        @Override
        public String getFamily() {
            return null;
        }

        @Override
        public void outputLink(FacesContext context, ResponseWriter writer, Object element, boolean isDetailed, String contextPath)
                throws IOException {

        }
    }

    //Stub implementation for test
    class LinkDataProviderTest implements LinkDataProvider {

        @Override
        public List<Class<?>> getSupportedClasses() {
            return null;
        }

        @Override
        public String getLabel(Object o, boolean detailed) {
            return null;
        }

        @Override
        public String getLink(Object o) {
            if (((String) o).equals("with-slash")) {
                return "/any/link";
            } else {
                return "any/link";
            }
        }

        @Override
        public String getTooltip(Object o) {
            return null;
        }
    }

}
