package net.ihe.gazelle.common.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ceoche
 */
public class ZipCreationTest {

    @Test
    public void unzipTest() throws IOException {

        ZipCreation.Unzip("src/test/resources/test.zip");

        // test files have been extracted
        File test = new File("test.txt");
        File anothertest = new File("another-test.txt");
        Assert.assertTrue("File test.txt should have been unzipped", test.exists());
        Assert.assertTrue("File another-test.txt should have been unzipped", anothertest.exists());


        // clean up
        test.delete();
        anothertest.delete();
    }

    @Test(expected = FileNotFoundException.class)
    public void unzipErrorTest() throws IOException {
        ZipCreation.Unzip("src/test/resources/test-does-not-exist.zip");
    }


    @Test
    public void files2ZipBytesTest() {
        List<String> files = new ArrayList<>();
        files.add("src/test/resources/test.txt");
        files.add("src/test/resources/another-test.txt");
        byte[] output = ZipCreation.files2zip(files);
        Assert.assertTrue(output.length > 370);
    }

    @Test
    public void files2ZipBytesErrorTest() {
        List<String> files = new ArrayList<>();
        files.add("src/test/resources/test.txt");
        files.add("src/test/resources/does-not-exist.txt");
        byte[] output = ZipCreation.files2zip(files);
        Assert.assertNull(output);
    }

    @Test
    public void files2ZipBytesErrorTest2() {
        byte[] output = ZipCreation.files2zip(null);
        Assert.assertNull(output);
    }

    @Test
    public void files2zipTest() throws IOException {

        List<String> files = new ArrayList<>();
        files.add("src/test/resources/test.txt");
        files.add("src/test/resources/another-test.txt");
        ZipCreation.files2zip("test.zip", files);

        // Verify zip have been created
        File output = new File("test.zip");
        Assert.assertTrue("File test.zip should have been created", output.exists());

        // clean up
        output.delete();

    }

    @Test
    public void files2zipErrorTest() throws IOException {

        List<String> files = new ArrayList<>();
        files.add("src/test/resources/does-not-exist.txt");
        ZipCreation.files2zip("test.zip", files);

        // Verify zip have NOT been created
        File output = new File("test.zip");
        Assert.assertFalse("File test.zip should not have been created", output.exists());

    }

}
