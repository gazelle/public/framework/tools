/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.common.tag;

import org.junit.Before;
import org.junit.Test;
import org.owasp.html.PolicyFactory;

import static org.junit.Assert.assertEquals;

/**
 * <b>Class Description : </b>SafeHtmlComponentTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 19/04/16
 * @class SafeHtmlComponentTest
 * @package net.ihe.gazelle.common.tag
 */

public class SafeHtmlComponentTest {

    private PolicyFactory policy;

    @Before
    public void setup() {
        SafeHtmlComponent shc = new SafeHtmlComponent();
        policy = shc.getPolicy();
    }

    @Test
    public void sinitizeH1() throws Exception {
        String org = "<h1>Gazelle</h1>";
        String sanitize = policy.sanitize(org);
        assertEquals(org, sanitize);
    }

    @Test
    public void sanitizeImage() throws Exception {
        String org = "<img src=\"https://gazelle.ihe.net/img/IHE_new.png\">";
        String expected = "<img src=\"https://gazelle.ihe.net/img/IHE_new.png\" />";
        String sanitize = policy.sanitize(org);
        assertEquals(expected, sanitize);
    }

    @Test
    public void sanitizeImageBase64() throws Exception {
        String org = "<img src=\"data:image/png;base64," +
                "iVBORw0KGgoAAAANSUhEUgAAAHEAAAAaCAYAAACATbNJAAAABHNCSVQICAgIfAhkiAAAABB0RVh0U29mdHdhcmUAU2h1dHRlcmOC0AkAAAXMSURBVGje7ZhtTFNXGMd" +
                "/DNLbxNtKbJuBkEi06zSrWWPIKInIPgBBMZkwZ2rCW6Yhjs2okwUSQSNTIwuGGefLnG44SUQ0jCzgiPBBRKeYalgkkVUkkqCwWQz2dklvh3EfWuRF2mIGWzT3/6nteXqe" +
                "/3P+53nJCfP+8cOzsLAwFLy6eEM5gtdARCULlUxUoIioQBFRgSKiIuKrCKmH7KRjnBnyfS5ceYIzD1/iP/8pPDR9vIePLnpCWkZMb8MR7l28xIETdlr7ZCAc" +
                "/TsW9h9czQrN7Iby6FoXvSYzCboZ3lgTRd6294nRvfp3c1oi9jfW8EGFh6yyfK4m6hG8Tjq7PCzRzDa9YZoOX8K5ZxZERCQhzTg9U9WrLqJ8n8qqfhLKtlKeJvp" +
                "/jGJF8jgTh53i0lYu9MkgvklmsY3ytEgEqYfsdVdIWq2mqW0Q59AIMamrOVKip37dCW58upWTyWr/Hs2kfzLM7gabP7vdtJRWs+/uE8jZzxkxjqo6GwkDAXxNpj2BUwwFR" +
                "/Mpip5UJle1ktGwifWqQDwXYwDweug8XM3xlj4eeAXezVzzfC1g7Ixwr" +
                "/48277roffxU4heRNFXNvJNESD1sCHHTlZhJNUVHfy24EOufm9GuNZKYflVbjwOJ2aZhST3DPVEua+bG944bIliQJF3bmlFyt3I7Y5d2I9a6K2o4ZBjxLf+uI9alnOubiu" +
                "/NtiwXG+g5KKajKxIOmt7cPnLdW9jN3KilQTNWKaklq0lc95cCk6XcPMXGytUIXxN4uS05WPv2IX99BryFoS4r1Py9J" +
                "+i9wntLKeufReOC2tZ2OZfCxp7BAZzPOU1JXR1FHPSOkhl1Wi8IA/8zt5aKDhdzO2jZgxSDyWldrTbtuDoKKUuF9r7ns7QYCO5kUURQ4CSIt" +
                "+10y6YKUrVIwBaUzxFy9w0NTqRAVRzsWXE+jJFE4stU6SzaRBDqpWFDjvtQ4DspP7yCEmZsS9k1Ev5Gm" +
                "+HmR0ZUWgB7Xw9BiFUyZya52gMWblGX1bqjBTYfGuuEHy0JiMWXQSgxpIchSANPxcR1RySPk8hdb4aQQDZ0UmHyszm5EgADInLyVsQPkPlVCMiuJ24vDDVCctDblyqWDTC2JbaODWugdGpSo123AXQ6iKQJQ+yzki+qZnqa25STJ20ykaqzMHphPY1zk4z3m46CMATNaAmZoo1Z1A+bjpONXOoZRAXEeB2IqmME3qyZd5YvLLbjTyBsxqDOEPlVIg2YlH1U3tr6lFX0EWilZxI8tgk67rvQTtP7f/uptc98nzN6fAgaNQIiCTZ4nhQ301HYzckW1kS4tBD+/LbiSLCBLvpIBBP37j/YFx/cvX51vRB+LjazrPhJzWbj37GzzWbOFccxwtzoDCJs3d43F4eXDPVE9HEsT1XpLW0hr1tgzySR5ClYe50OXEBwlvxpAhdVLb4SojLcZ3KW2pSVvtKDN6/qP+xh34Z5IfdHGpyY8mI8pUfq5X3Bq5Q2QIpWfoXE11QYxA83HH4ognpa/RvJgtJ3i4qm3x28pCTe1KIOIPwxPuE+tpuHgEMdXO80YMlIwptED5IHlBFolf520XtfYJREExmEqRODrUN+/a6eZ1T0+yJEdMxWZS3kTqhkZ37v+XUY4BwYqzpnD6oRyvEsuPrdHaWnmDpl/4JbVs2RaYIkHz9JEW0k5Ny1j/ZreXI6JQrxJKf6GHdrXiqTFNR0WPbGEdO2QFMFYuoqssO7GvCxTNSfnA5JeXHWFrxFMQYtp/MZ5EueE+ckqcEQvTbFOjsrEs6OzadpomAGJCPEJ3O+vpaVq64hCY6lqyNVhbXB0uWxezeY6Gw/CBLy8KJscZjs86hfRoihv39Z/WzWX0hGR3jdVO/Slz+4hv2mfNpztP/vy85QXkqz26BBxWHncpbkRSk6lEwyy82M6+ek8oNxzjeF8mq4myy5itC/BvMbjlV8PqXUwWKiAoUERURFSgiKphJ/ANfKtPbhgJS9gAAAABJRU5ErkJggg==\" data-filename=\"Infobulle_004.png\" style=\"width: 113px;\">";
        String expected = "<img src=\"data:image/png;base64," +
                "iVBORw0KGgoAAAANSUhEUgAAAHEAAAAaCAYAAACATbNJAAAABHNCSVQICAgIfAhkiAAAABB0RVh0U29mdHdhcmUAU2h1dHRlcmOC0AkAAAXMSURBVGje7ZhtTFNXGMd" +
                "/DNLbxNtKbJuBkEi06zSrWWPIKInIPgBBMZkwZ2rCW6Yhjs2okwUSQSNTIwuGGefLnG44SUQ0jCzgiPBBRKeYalgkkVUkkqCwWQz2dklvh3EfWuRF2mIGWzT3/6nteXqe" +
                "/3P&#43;53nJCfP&#43;8cOzsLAwFLy6eEM5gtdARCULlUxUoIioQBFRgSKiIuKrCKmH7KRjnBnyfS5ceYIzD1/iP/8pPDR9vIePLnpCWkZMb8MR7l28xIETdlr7ZCAc" +
                "/TsW9h9czQrN7Iby6FoXvSYzCboZ3lgTRd6294nRvfp3c1oi9jfW8EGFh6yyfK4m6hG8Tjq7PCzRzDa9YZoOX8K5ZxZERCQhzTg9U9WrLqJ8n8qqfhLKtlKeJvp" +
                "/jGJF8jgTh53i0lYu9MkgvklmsY3ytEgEqYfsdVdIWq2mqW0Q59AIMamrOVKip37dCW58upWTyWr/Hs2kfzLM7gabP7vdtJRWs&#43;" +
                "/uE8jZzxkxjqo6GwkDAXxNpj2BUwwFR/Mpip5UJle1ktGwifWqQDwXYwDweug8XM3xlj4eeAXezVzzfC1g7Ixwr" +
                "/48277roffxU4heRNFXNvJNESD1sCHHTlZhJNUVHfy24EOufm9GuNZKYflVbjwOJ2aZhST3DPVEua&#43;bG944bIliQJF3bmlFyt3I7Y5d2I9a6K2o4ZBjxLf&#43;" +
                "uI9alnOubiu/NtiwXG&#43;g5KKajKxIOmt7cPnLdW9jN3KilQTNWKaklq0lc95cCk6XcPMXGytUIXxN4uS05WPv2IX99BryFoS4r1Py9J&#43;" +
                "i9wntLKeufReOC2tZ2OZfCxp7BAZzPOU1JXR1FHPSOkhl1Wi8IA/8zt5aKDhdzO2jZgxSDyWldrTbtuDoKKUuF9r7ns7QYCO5kUURQ4CSIt&#43;" +
                "10y6YKUrVIwBaUzxFy9w0NTqRAVRzsWXE&#43;jJFE4stU6SzaRBDqpWFDjvtQ4DspP7yCEmZsS9k1Ev5Gm&#43;" +
                "HmR0ZUWgB7Xw9BiFUyZya52gMWblGX1bqjBTYfGuuEHy0JiMWXQSgxpIchSANPxcR1RySPk8hdb4aQQDZ0UmHyszm5EgADInLyVsQPkPlVCMiuJ24vDDVCctDblyqWDTC2JbaODWugdGpSo123AXQ6iKQJQ&#43;yzki&#43;qZnqa25STJ20ykaqzMHphPY1zk4z3m46CMATNaAmZoo1Z1A&#43;bjpONXOoZRAXEeB2IqmME3qyZd5YvLLbjTyBsxqDOEPlVIg2YlH1U3tr6lFX0EWilZxI8tgk67rvQTtP7f/uptc98nzN6fAgaNQIiCTZ4nhQ301HYzckW1kS4tBD&#43;/LbiSLCBLvpIBBP37j/YFx/cvX51vRB&#43;LjazrPhJzWbj37GzzWbOFccxwtzoDCJs3d43F4eXDPVE9HEsT1XpLW0hr1tgzySR5ClYe50OXEBwlvxpAhdVLb4SojLcZ3KW2pSVvtKDN6/qP&#43;xh34Z5IfdHGpyY8mI8pUfq5X3Bq5Q2QIpWfoXE11QYxA83HH4ognpa/RvJgtJ3i4qm3x28pCTe1KIOIPwxPuE&#43;tpuHgEMdXO80YMlIwptED5IHlBFolf520XtfYJREExmEqRODrUN&#43;/a6eZ1T0&#43;yJEdMxWZS3kTqhkZ37v&#43;XUY4BwYqzpnD6oRyvEsuPrdHaWnmDpl/4JbVs2RaYIkHz9JEW0k5Ny1j/ZreXI6JQrxJKf6GHdrXiqTFNR0WPbGEdO2QFMFYuoqssO7GvCxTNSfnA5JeXHWFrxFMQYtp/MZ5EueE&#43;ckqcEQvTbFOjsrEs6OzadpomAGJCPEJ3O&#43;vpaVq64hCY6lqyNVhbXB0uWxezeY6Gw/CBLy8KJscZjs86hfRoihv39Z/WzWX0hGR3jdVO/Slz&#43;4hv2mfNpztP/vy85QXkqz26BBxWHncpbkRSk6lEwyy82M6&#43;ek8oNxzjeF8mq4myy5itC/BvMbjlV8PqXUwWKiAoUERURFSgiKphJ/ANfKtPbhgJS9gAAAABJRU5ErkJggg&#61;&#61;\" data-filename=\"Infobulle_004.png\" style=\"width:113px\" />";
        String sanitize = policy.sanitize(org);
        assertEquals(expected, sanitize);
    }

    @Test
    public void sanitizeDiv() throws Exception {
        String org = "<div class=\"gzl-text\">We want to thank you all for your good spirit and good work during this week.</div>";
        String sanitize = policy.sanitize(org);
        assertEquals(org, sanitize);
    }

    @Test
    public void sanitizeSpan() throws Exception {
        String org = "<span class=\"gzl-text\">We want to thank you all for your good spirit and good work during this week.</span>";
        String sanitize = policy.sanitize(org);
        assertEquals(org, sanitize);
    }

    @Test
    public void sanitizeDivStyle() throws Exception {
        String org = "<p class=\"p1\" style=\"font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: " +
                "18.5714px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; color: rgb(51, 51, 51);\"><br></p>";
        String expected = "<p class=\"p1\" style=\"font-style:normal;font-variant:normal;font-weight:normal;font-size:13px;line-height:18.5714px;" +
                "font-family:&#39;helvetica neue&#39; , &#39;helvetica&#39; , &#39;arial&#39; , sans-serif;color:rgb( 51 , 51 , 51 )\"><br /></p>";
        String sanitize = policy.sanitize(org);
        assertEquals(expected, sanitize);
    }

    @Test
    public void sanitizeP() throws Exception {
        String org = "<p class=\"p1\" style=\"font-style: normal; font-variant: normal; font-weight: normal; font-size: 13px; line-height: " +
                "18.5714px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; color: rgb(51, 51, 51);\"><span style=\"color: rgb(0, 0, " +
                "0);\"><br></span></p>";
        String expected = "<p class=\"p1\" style=\"font-style:normal;font-variant:normal;font-weight:normal;font-size:13px;line-height:18.5714px;" +
                "font-family:&#39;helvetica neue&#39; , &#39;helvetica&#39; , &#39;arial&#39; , sans-serif;color:rgb( 51 , 51 , 51 )\"><span " +
                "style=\"color:rgb( 0 , 0 , 0 )\"><br /></span></p>";
        String sanitize = policy.sanitize(org);
        assertEquals(expected, sanitize);
    }

    @Test
    public void sanitizeP2() throws Exception {
        String org = "<p class=\"MsoNormal\"><span><font style=\"text-decoration:underline\">Patient Demographics</font></span></p>";
        String expected = "<p class=\"MsoNormal\"><font style=\"text-decoration:underline\">Patient Demographics</font></p>";
        String sanitize = policy.sanitize(org);
        assertEquals(expected, sanitize);
    }

    @Test
    public void sanitizeUl() throws Exception {
        String org = "<ul>\n" +
                "   <li><a href=\"https://gazelle.ihe.net/training#EU2016training\">https://gazelle.ihe" +
                ".net/training#EU2016training</a><a href=\"http://gazelle.ihe.net/training#EU2016training\"></a></li>\n" +
                "</ul>";
        String expected = "<ul><li><a href=\"https://gazelle.ihe.net/training#EU2016training\" rel=\"nofollow\">https://gazelle.ihe" +
                ".net/training#EU2016training</a><a href=\"http://gazelle.ihe.net/training#EU2016training\" rel=\"nofollow\"></a></li></ul>";
        String sanitize = policy.sanitize(org);
        assertEquals(expected, sanitize);
    }

    @Test
    public void sanitizeDesc() throws Exception {
        String org = "<h2>Special Instructions</h2>\n" +
                "\n" +
                "<p>In this test, you demonstrate that your system is able to perform secured transactions over different types of transports, (eg " +
                "HL7v2 DICOM, webservices, syslog). The types of transport you support depends on your implementation, and they are identified " +
                "according to the answers in your ATNA Questionnaire.<strong><em> So, you must first complete the " +
                "</em><em><strong>ATNA_Questionnaire </strong></em><em>test before performing this one.</em></strong></p>\n" +
                "\n" +
                "<ul>\n" +
                "\n" +
                "</ul>\n" +
                "\n" +
                "<p><strong>Frequently Asked Question</strong>:&nbsp;<em> \"The PIX profile does not require that I support ATNA; why do I have to " +
                "secure my HL7v2 connection?\"&nbsp;</em> or <em>\"The SWF.b profile does not require that I support ATNA; why do I have to secure " +
                "my DICOM connection?\"</em></p>\n" +
                "\n" +
                "<p><strong>Frequently Supplied Answer:&nbsp; </strong><em>\"Yes, you're right; PIX (or SWF, or other profiles), does not require " +
                "that you support ATNA.&nbsp; However, you have declared that your system is a Secure Node or Application in the ATNA profile" +
                ".&nbsp; It is the ATNA profile that requires you ***to be able to*** secure all inbound and outbound connections that carry PHI" +
                ".&nbsp; Thus, in order to pass ATNA, you must demonstrate at the Connectathon that you are capable of doing this.&nbsp; You do not" +
                " need to do all of your HL7v2 or DICOM testing at Connectathon over TLS, but you need to demonstrate in this test that you can " +
                "secure these types of transport.</em>\" (Ref ITI TF-1:9.1.2&nbsp; and 9.7).</p>\n" +
                "\n" +
                "<h2>Description</h2>\n" +
                "\n" +
                "<p>This&nbsp;<strong>ATNA_Authentication_with_Tool&nbsp;</strong>test verifies that Secure Nodes or Secure Applications (SN/SA)" +
                "&nbsp; communicate using TLS and exchange the proper certificates.</p>\n" +
                "\n" +
                "<p>If you completed pre-Connectathon test " +
                "<a href=\"http://gazelle.ihe.net/content/pre-connectathon-tests/atna/11109\" target=\"_blank\">11109</a>, then you have completed " +
                "the <strong>TLS tests </strong>tab of your ATNA Questionnaire. &nbsp;Well done -- there is no need to re-do that work during " +
                "connectathon week. &nbsp;You can mark this test \"To be verified\".</p>\n" +
                "\n" +
                "<p>If the <strong>TLS tests</strong>&nbsp;tab on your ATNA Questionnaire is not complete, then follow the link to test " +
                "<a href=\"http://gazelle.ihe.net/content/pre-connectathon-tests/atna/11109\" target=\"_blank\">11109</a> and do the work now" +
                ".</p>\n" +
                "\n" +
                "<h2>Evaluation</h2>\n" +
                "\n" +
                "<p>The Connectathon Monitor reviews <strong>TLS tests </strong>tab on the ATNA Questionnaire as follows. &nbsp;<em>&nbsp;" +
                "</em></p>\n" +
                "\n" +
                "<p>The Monitor will:</p>\n" +
                "\n" +
                "<ol>\n" +
                "\n" +
                "<li>Find the questionnaire for this test system&nbsp;in the TLS Tool for the Connectathon under menu <em>Audit Trail--&gt;ATNA " +
                "Questionnaire. </em>(There should also be a direct link in step 40 of this test instance.)</li>\n" +
                "\n" +
                "<li>Verify that the questionnaire is marked with status 'Ready to be reviewed'. &nbsp;If not, mark this test 'Partially verified' " +
                "with a note to the vendor to complete the questionnaire and mark it 'Ready to be reviewed'. &nbsp;We do not want to waste " +
                "monitors' time reviewing incomplete questionnaires.</li>\n" +
                "\n" +
                "<li>Access the <strong>TLS tests</strong>&nbsp;tab in the questionnaire.</li>\n" +
                "\n" +
                "<li>For each \"SERVER\" tested side :                                 \n" +
                "<ul>\n" +
                "\n" +
                "<li>the test result must be PASSED (\"blue circle\" on page with connection details)</li>\n" +
                "\n" +
                "<li>In the connection detail, the cipher suite must be TLS_RSA_WITH_AES_128_CBC_SHA and the protocol TLSv1.</li>\n" +
                "\n" +
                "</ul>\n" +
                "\n" +
                "</li>\n" +
                "\n" +
                "<li>For each \"CLIENT\" tested side :                                 \n" +
                "<ul>\n" +
                "\n" +
                "<li>the connection must succeed (\"blue circle\" on page with connection details).</li>\n" +
                "\n" +
                "<li>The cipher suite must be TLS_RSA_WITH_AES_128_CBC_SHA.</li>\n" +
                "\n" +
                "<li>The protocol must be TLSv1.</li>\n" +
                "\n" +
                "</ul>\n" +
                "\n" +
                "</li>\n" +
                "\n" +
                "<li>The monitor may choose to ask the vendor to re-run a test if the results raise questions about the system's support TLS" +
                ".</li>\n" +
                "\n" +
                "</ol>\n" +
                "\n" +
                "<p>If any of these requirement fails, the Monitor should mark the test \"partially verified\" and indicate in the chat box what is" +
                " wrong or missing.</p>";
        String expected = "<h2>Special Instructions</h2>\n" +
                "\n" +
                "<p>In this test, you demonstrate that your system is able to perform secured transactions over different types of transports, (eg " +
                "HL7v2 DICOM, webservices, syslog). The types of transport you support depends on your implementation, and they are identified " +
                "according to the answers in your ATNA Questionnaire.<strong><em> So, you must first complete the " +
                "</em><em><strong>ATNA_Questionnaire </strong></em><em>test before performing this one.</em></strong></p>\n" +
                "\n" +
                "<ul></ul>\n" +
                "\n" +
                "<p><strong>Frequently Asked Question</strong>: <em> &#34;The PIX profile does not require that I support ATNA; why do I have to " +
                "secure my HL7v2 connection?&#34; </em> or <em>&#34;The SWF.b profile does not require that I support ATNA; why do I have to secure" +
                " my DICOM connection?&#34;</em></p>\n" +
                "\n" +
                "<p><strong>Frequently Supplied Answer:  </strong><em>&#34;Yes, you&#39;re right; PIX (or SWF, or other profiles), does not require" +
                " that you support ATNA.  However, you have declared that your system is a Secure Node or Application in the ATNA profile.  It is " +
                "the ATNA profile that requires you ***to be able to*** secure all inbound and outbound connections that carry PHI.  Thus, in order" +
                " to pass ATNA, you must demonstrate at the Connectathon that you are capable of doing this.  You do not need to do all of your " +
                "HL7v2 or DICOM testing at Connectathon over TLS, but you need to demonstrate in this test that you can secure these types of " +
                "transport.</em>&#34; (Ref ITI TF-1:9.1.2  and 9.7).</p>\n" +
                "\n" +
                "<h2>Description</h2>\n" +
                "\n" +
                "<p>This <strong>ATNA_Authentication_with_Tool </strong>test verifies that Secure Nodes or Secure Applications (SN/SA)  communicate" +
                " using TLS and exchange the proper certificates.</p>\n" +
                "\n" +
                "<p>If you completed pre-Connectathon test <a " +
                "href=\"http://gazelle.ihe.net/content/pre-connectathon-tests/atna/11109\" target=\"_blank\" rel=\"nofollow\">11109</a>, then you " +
                "have completed the <strong>TLS tests </strong>tab of your ATNA Questionnaire.  Well done -- there is no need to re-do that work " +
                "during connectathon week.  You can mark this test &#34;To be verified&#34;.</p>\n" +
                "\n" +
                "<p>If the <strong>TLS tests</strong> tab on your ATNA Questionnaire is not complete, then follow the link to test <a " +
                "href=\"http://gazelle.ihe.net/content/pre-connectathon-tests/atna/11109\" target=\"_blank\" rel=\"nofollow\">11109</a> and do the " +
                "work now.</p>\n" +
                "\n" +
                "<h2>Evaluation</h2>\n" +
                "\n" +
                "<p>The Connectathon Monitor reviews <strong>TLS tests </strong>tab on the ATNA Questionnaire as follows.  <em> </em></p>\n" +
                "\n" +
                "<p>The Monitor will:</p>\n" +
                "\n" +
                "<ol><li>Find the questionnaire for this test system in the TLS Tool for the Connectathon under menu <em>Audit Trail--&gt;ATNA " +
                "Questionnaire. </em>(There should also be a direct link in step 40 of this test instance.)</li><li>Verify that the questionnaire " +
                "is marked with status &#39;Ready to be reviewed&#39;.  If not, mark this test &#39;Partially verified&#39; with a note to the " +
                "vendor to complete the questionnaire and mark it &#39;Ready to be reviewed&#39;.  We do not want to waste monitors&#39; time " +
                "reviewing incomplete questionnaires.</li><li>Access the <strong>TLS tests</strong> tab in the questionnaire.</li><li>For each " +
                "&#34;SERVER&#34; tested side :                                 \n" +
                "<ul><li>the test result must be PASSED (&#34;blue circle&#34; on page with connection details)</li><li>In the connection detail, " +
                "the cipher suite must be TLS_RSA_WITH_AES_128_CBC_SHA and the protocol TLSv1.</li></ul>\n" +
                "\n" +
                "</li><li>For each &#34;CLIENT&#34; tested side :                                 \n" +
                "<ul><li>the connection must succeed (&#34;blue circle&#34; on page with connection details).</li><li>The cipher suite must be " +
                "TLS_RSA_WITH_AES_128_CBC_SHA.</li><li>The protocol must be TLSv1.</li></ul>\n" +
                "\n" +
                "</li><li>The monitor may choose to ask the vendor to re-run a test if the results raise questions about the system&#39;s support " +
                "TLS.</li></ol>\n" +
                "\n" +
                "<p>If any of these requirement fails, the Monitor should mark the test &#34;partially verified&#34; and indicate in the chat box " +
                "what is wrong or missing.</p>";
        String sanitize = policy.sanitize(org);
        assertEquals(expected, sanitize);
    }

    @Test
    public void sanitizeScript() throws Exception {
        String org = "<script>alert(\"Hell yeah\");</script>";
        String sanitize = policy.sanitize(org);
        assertEquals("", sanitize);
    }
}