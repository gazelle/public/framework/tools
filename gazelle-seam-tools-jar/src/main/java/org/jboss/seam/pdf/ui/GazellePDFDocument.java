package org.jboss.seam.pdf.ui;

import java.io.OutputStream;
import java.util.Date;

import javax.faces.context.FacesContext;

import com.lowagie.text.DocWriter;
import com.lowagie.text.DocumentException;
import com.lowagie.text.html.HtmlWriter;
import com.lowagie.text.pdf.GazellePdfDocument;
import com.lowagie.text.pdf.GazellePdfWriter;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.rtf.RtfWriter2;

public class GazellePDFDocument extends UIDocument {

	@Override
	protected DocWriter createWriterForStream(OutputStream stream) throws DocumentException {
		if (documentType == PDF) {
			GazellePdfDocument.CREATION_DATE.set(getCreationDate());
			PdfDocument pdf = new GazellePdfDocument();
			document.addDocListener(pdf);
			PdfWriter writer = new GazellePdfWriter(pdf, stream);
			pdf.addWriter(writer);
			return writer;
		} else if (documentType == RTF) {
			return RtfWriter2.getInstance(document, stream);
		} else if (documentType == HTML) {
			return HtmlWriter.getInstance(document, stream);
		}

		throw new IllegalArgumentException("unknown document type");
	}

	protected Date getCreationDate() {
		return (Date) valueBinding(FacesContext.getCurrentInstance(), "creationDate", new Date());
	}

}
