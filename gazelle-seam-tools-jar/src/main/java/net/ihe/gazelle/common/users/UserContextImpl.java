package net.ihe.gazelle.common.users;

import net.ihe.gazelle.common.util.GazelleCookie;
import net.ihe.gazelle.users.UserContext;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("userContext")
@AutoCreate
@Scope(ScopeType.SESSION)
public class UserContextImpl implements UserContext {

    private static final String GZL_CURRENT_TESTING_SESSION_ID = "GZL_CURRENT_TESTING_SESSION_ID";
    private Integer testingSessionId;

    @Override
    public Integer getSelectedTestingSessionId() {
        if (testingSessionId == null) {
            String cookie = GazelleCookie.getCookie(GZL_CURRENT_TESTING_SESSION_ID);
            testingSessionId = cookie == null ? null : Integer.parseInt(cookie);
        }
        return testingSessionId;
    }

    @Override
    public void setSelectedTestingSessionId(Integer testingSessionId) {
        this.testingSessionId = testingSessionId;

        if (testingSessionId != null)
            GazelleCookie.setCookie(GZL_CURRENT_TESTING_SESSION_ID, String.valueOf(testingSessionId));
        else
            GazelleCookie.deleteCookie(GZL_CURRENT_TESTING_SESSION_ID, "", "/");
        
    }
}
