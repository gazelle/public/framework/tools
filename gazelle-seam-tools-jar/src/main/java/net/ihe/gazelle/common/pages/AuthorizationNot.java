package net.ihe.gazelle.common.pages;

public class AuthorizationNot implements Authorization {

	private Authorization authorization;

	public AuthorizationNot(Authorization authorization) {
		super();
		this.authorization = authorization;
	}

	@Override
	public boolean isGranted(Object... context) {
		return !authorization.isGranted(context);
	}

}
