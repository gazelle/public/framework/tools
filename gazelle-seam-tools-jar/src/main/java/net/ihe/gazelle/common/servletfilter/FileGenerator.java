package net.ihe.gazelle.common.servletfilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface FileGenerator {

	String getPath();

	void process(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);

}
