package net.ihe.gazelle.common.filter.criterion;

import java.io.Serializable;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import net.ihe.gazelle.common.util.DateDisplayUtil;
import net.ihe.gazelle.hql.criterion.IntervalType;
import net.ihe.gazelle.hql.criterion.date.DateValue;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@BypassInterceptors
@Name("dateValueConvertor")
@Converter(forClass = DateValue.class)
public class DateValueConvertor implements javax.faces.convert.Converter, Serializable {

	private static final long serialVersionUID = 739049097980035837L;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		DateValue result = new DateValue(DateDisplayUtil.getTimeZone());
		if (value != null) {
			String[] split = StringUtils.split(value, '|');

			result.setIntervalType(IntervalType.valueOf(split[0]));
			result.setValue1(new Date(Long.parseLong(split[1])));
			result.setValue2(new Date(Long.parseLong(split[2])));
		}
		return result;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if ((value != null) && (value instanceof DateValue)) {
			DateValue dateValue = (DateValue) value;
			return dateValue.getIntervalType().name() + "|" + dateValue.getValue1().getTime() + "|"
					+ dateValue.getValue2().getTime();
		}
		return null;
	}
}
