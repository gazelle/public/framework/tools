/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.filter;

import com.sun.faces.facelets.tag.ui.ComponentRef;
import net.ihe.gazelle.common.filter.action.DatatableStateHolderBean;
import net.ihe.gazelle.common.filter.criterion.HQLValueFormatter;
import net.ihe.gazelle.common.filter.criterion.ValueFormatter;
import net.ihe.gazelle.common.filter.util.MapNotifier;
import net.ihe.gazelle.common.filter.util.MapNotifierListener;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.NullValue;
import net.ihe.gazelle.hql.beans.HQLStatistic;
import net.ihe.gazelle.hql.beans.HQLStatisticItem;
import net.ihe.gazelle.hql.criterion.*;
import net.ihe.gazelle.hql.criterion.date.DateCriterion;
import net.ihe.gazelle.hql.criterion.date.DateValue;
import net.ihe.gazelle.hql.criterion.number.NumberValue;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.jboss.seam.Component;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.component.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.Map.Entry;

public class Filter<E> implements MapNotifierListener, Serializable {

    private static final long serialVersionUID = -2929998211522068806L;

    private static Logger log = LoggerFactory.getLogger(Filter.class);

    protected Class<E> entityClass;
    protected List<QueryModifier<E>> queryModifiers;
    private String updatedField;
    protected Map<String, HQLCriterion<E, ?>> criterions;

    // Value selected for each criterion
    protected MapNotifier filterValues;

    // Used for list with statistics
    private boolean refreshListStatistics;
    protected List<HQLStatisticItem> statisticItems;
    private transient List<HQLStatistic<E>> listWithStatistics;

    // String value for each criterion (only reliable on read)
    private boolean refreshStringValues;
    protected transient Map<String, Object> stringValues;

    // Data model calling this filter, can be null
    // Used to back call the data model on getting possible values)
    private FilterDataModel<E> filterDataModel = null;

    private Map<String, List<Object>> possibleValuesWrapper;
    private transient Map<Keyword, List<Object>> possibleValues;
    private transient Map<Keyword, List<Integer>> possibleValuesCount;

    private transient Map<String, Converter> converters;

    private Map<String, ValueFormatter> formatters;

    private Map<String, Suggester<E, Object>> suggesters;

    private Map<String, List<QueryModifier<E>>> queryModifiersForSuggest;

    private EntityManager entityManager;

    private FilterUpdateCallback listener;
    protected transient Map<String, String> reflexiveValues;

    private Map<String, Boolean> display;

    public void setReflexiveValues(Map<String, String> reflexiveValues) {
        this.reflexiveValues = reflexiveValues;
    }

    public Map<String, String> getReflexiveValues() {
        return reflexiveValues;
    }

    private Object getById(Class className, int id) {
        HQLQueryBuilder queryBuilder;
        if (getEntityManager() != null) {
            queryBuilder = new HQLQueryBuilder(getEntityManager(), className);
        } else {
            queryBuilder = new HQLQueryBuilder(className);
        }
        queryBuilder.addEq("id", id);
        return queryBuilder.getUniqueResult();
    }

    public void setDisplay(String filterId) {
        display.put(filterId, true);
    }

    public boolean isDisplay(String filterId) {
        return display.get(filterId) == null ? false : display.get(filterId);
    }

    public void updateFilterVal(String filterId) {
        String name = (String) getStringValues().get(filterId);

        if (name != null) {
            Object value = getSuggesters().get(filterId).find(name);
            filterValues.put(filterId, value);
        }

        setUpdatedField(filterId);
    }

    public FilterUpdateCallback getListener() {
        return listener;
    }

    public void setListener(FilterUpdateCallback listener) {
        this.listener = listener;
    }

    public EntityManager getEntityManager() {
        if ((entityManager == null)
                || ((entityManager != null) && !entityManager.isOpen())) {
            entityManager = EntityManagerService.provideEntityManager();
        }
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public static final <E, F> String getSelectableLabel(
            HQLCriterion<E, F> effectiveFilter, F value) {
        if (NullValue.NULL_VALUE.equals(value)) {
            return "Not defined";
        } else {
            String result = effectiveFilter.getSelectableLabel(value);
            result = StatusMessage.getBundleMessage(result, result);
            return result;
        }
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        in.defaultReadObject();
        modified();
        converters = new HashMap<String, Converter>();
    }

    public Filter(Class<E> entityClass,
                  List<HQLCriterion<E, ?>> criterionsList,
                  Map<String, String> requestParameterMap) {
        this(entityClass, new ArrayList<QueryModifier<E>>(), criterionsList,
                null, requestParameterMap);
    }

    public Filter(Class<E> entityClass, List<HQLCriterion<E, ?>> criterionsList) {
        this(entityClass, new ArrayList<QueryModifier<E>>(), criterionsList,
                null, null);
    }

    public Filter(Class<E> entityClass, QueryModifier<E> queryModifier,
                  List<HQLCriterion<E, ?>> criterionsList,
                  Map<String, String> requestParameterMap) {
        this(entityClass, Arrays.asList(queryModifier), criterionsList, null,
                requestParameterMap);
    }

    public Filter(Class<E> entityClass, QueryModifier<E> queryModifier,
                  List<HQLCriterion<E, ?>> criterionsList) {
        this(entityClass, Arrays.asList(queryModifier), criterionsList, null,
                null);
    }

    public Filter(HQLCriterionsForFilter<E> hqlCriterions) {
        this(hqlCriterions.getEntityClass(), hqlCriterions.getQueryModifiers(),
                hqlCriterions.getCriterionsList(), hqlCriterions
                        .getQueryModifiersForSuggest(), null);
    }

    public Filter(HQLCriterionsForFilter<E> hqlCriterions,
                  Map<String, String> requestParameterMap) {
        this(hqlCriterions.getEntityClass(), hqlCriterions.getQueryModifiers(),
                hqlCriterions.getCriterionsList(), hqlCriterions
                        .getQueryModifiersForSuggest(), requestParameterMap);
    }

    public Filter(HQLCriterionsForFilter<E> hqlCriterions,
                  Map<String, String> requestParameterMap, EntityManager entityManager) {
        this(hqlCriterions.getEntityClass(), hqlCriterions.getQueryModifiers(),
                hqlCriterions.getCriterionsList(), hqlCriterions
                        .getQueryModifiersForSuggest(), requestParameterMap);
        setEntityManager(entityManager);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Filter(Class<E> entityClass, List<QueryModifier<E>> queryModifiers,
                  List<HQLCriterion<E, ?>> criterionsList,
                  Map<String, List<QueryModifier<E>>> queryModifiersForSuggestParam,
                  Map<String, String> requestParameterMap) {
        super();
        this.entityClass = entityClass;
        this.queryModifiers = queryModifiers;
        markRefresh();

        resetPossibleValues();
        possibleValuesWrapper = new MapPossibleValuesWrapper(this);

        stringValues = null;

        filterValues = new MapNotifier(new HashMap<String, Object>());
        filterValues.addListener(this);

        suggesters = new HashMap<String, Suggester<E, Object>>();
        formatters = new HashMap<String, ValueFormatter>();
        converters = new HashMap<String, Converter>();
        display = new HashMap<String, Boolean>();
        queryModifiersForSuggest = new HashMap<String, List<QueryModifier<E>>>();

        criterions = new HashMap<String, HQLCriterion<E, ?>>();

        for (HQLCriterion<E, ?> criterion : criterionsList) {
            List<QueryModifier<E>> queryModifiersForSuggestForCriterion = null;
            if (queryModifiersForSuggestParam != null) {
                queryModifiersForSuggestForCriterion = queryModifiersForSuggestParam
                        .get(criterion.getKeyword());
            }

            addCriterion(criterion, queryModifiersForSuggestForCriterion);
        }

        clear();

        if (requestParameterMap != null) {
            initFromUrl(requestParameterMap);
        }
    }

    public void addCriterion(HQLCriterion<E, ?> criterion,
                             List<QueryModifier<E>> queryModifiersForSuggestForCriterion) {
        String keyword = criterion.getKeyword();

        filterValues.put(keyword, null);
        suggesters.put(keyword, new Suggester(this, keyword, criterion));
        formatters.put(keyword, new HQLValueFormatter(this, keyword));
        criterions.put(keyword, criterion);
        if (queryModifiersForSuggestForCriterion != null) {
            queryModifiersForSuggest.put(keyword,
                    queryModifiersForSuggestForCriterion);
        }
    }

    public void appendHibernateFilters(HQLQueryBuilder<E> queryBuilder) {
        appendHibernateFilters(queryBuilder, null);
    }

    public void appendHibernateFilters(HQLQueryBuilder<E> queryBuilder,
                                       String excludedKeyword) {
        appendHibernateFilters(queryBuilder, excludedKeyword, null);
    }

    public void appendHibernateFilters(HQLQueryBuilder<E> queryBuilder,
                                       String excludedKeyword, String countedKeyword) {

        Set<QueryModifier<E>> realQueryModifiers = new HashSet<QueryModifier<E>>();

        if (queryModifiers != null) {
            realQueryModifiers.addAll(queryModifiers);
        }

        Set<String> filteredKeyword = new HashSet<String>();
        if (countedKeyword != null) {
            filteredKeyword.add(countedKeyword);
        }

        Map<String, Object> filterValuesApplied = new HashMap<String, Object>();

        for (HQLCriterion<E, ?> effectiveFilter : criterions.values()) {
            // The value is provided
            ValueProvider valueFixer = effectiveFilter.getValueFixer();
            Object fixedValue = null;
            if (valueFixer != null) {
                try {
                    fixedValue = valueFixer.getValue();
                } catch (Exception e) {
                    log.error("Failed to get fix falue for criterion "
                            + effectiveFilter.getKeyword(), e);
                }
            }
            if (fixedValue != null) {
                String path = effectiveFilter.getPath();
                queryBuilder.addEq(path, fixedValue);
                filterValuesApplied.put(effectiveFilter.getKeyword(),
                        fixedValue);
                filteredKeyword.add(effectiveFilter.getKeyword());
            } else {
                // The value can be selected
                String keyword = effectiveFilter.getKeyword();
                if (!keyword.equals(excludedKeyword)) {
                    Object filterValue = filterValues.get(keyword);
                    if (filterValue != null) {
                        filterValuesApplied.put(keyword, filterValue);
                        effectiveFilter.filter(queryBuilder, filterValue);
                        filteredKeyword.add(effectiveFilter.getKeyword());
                    }
                }
            }
        }

        for (String keyword : filteredKeyword) {
            if (queryModifiersForSuggest.get(keyword) != null) {
                List<QueryModifier<E>> queryModifiersForThisSuggest = queryModifiersForSuggest
                        .get(keyword);
                realQueryModifiers.addAll(queryModifiersForThisSuggest);
            }
        }

        for (QueryModifier<E> queryModifier : realQueryModifiers) {
            if (queryModifier != null) {
                queryModifier.modifyQuery(queryBuilder, filterValuesApplied);
            }
        }
    }

    public void clear() {
        filterValues.clear();
        for (HQLCriterion<E, ?> effectiveFilter : criterions.values()) {
            if (effectiveFilter.getValueInitiator() != null) {
                Object value = null;
                try {
                    value = effectiveFilter.getValueInitiator().getValue();
                } catch (Exception e) {
                    log.error("Failed to get initial falue for criterion "
                            + effectiveFilter.getKeyword(), e);
                }
                if (value != null) {
                    getFilterValues().put(effectiveFilter.getKeyword(), value);
                }
            }
        }
    }

    public Class<E> getEntityClass() {
        return entityClass;
    }

    public Converter getConverter(String keyword) {
        Converter converter = converters.get(keyword);
        if (converter == null) {
            HQLCriterion<E, ?> criterion = criterions.get(keyword);
            if (criterion == null) {
                return null;
            }

            Class<?> selectableClass = criterion.getSelectableClass();
            if (selectableClass != null) {
                if (selectableClass.equals(String.class)) {
                    converter = new StringConverter();
                } else {
                    try {
                        converter = FacesContext.getCurrentInstance()
                                .getApplication()
                                .createConverter(selectableClass);
                    } catch (Exception e) {
                        converter = null;
                        log.error("Failed to load converter for "
                                + selectableClass, e);
                    }
                }
                // Here we delegate the converter as we want to manage null as
                // value
                // and null as none selected
                converter = new ConverterDelegator(converter);
                converters.put(keyword, converter);
            }
        }
        return converter;
    }

    public Map<String, HQLCriterion<E, ?>> getCriterions() {
        return criterions;
    }

    public FilterDataModel<E> getFilterDataModel() {
        return filterDataModel;
    }

    public MapNotifier getFilterValues() {
        return filterValues;
    }

    public Map<String, ValueFormatter> getFormatters() {
        return formatters;
    }

    public List<HQLStatistic<E>> getListWithStatistics() {
        refreshListWithStatistics();
        return listWithStatistics;
    }

    public List<Object> getListWithStatisticsItems(HQLStatistic<E> item,
                                                   int statisticItemIndex) {
        HQLQueryBuilder<E> queryBuilder = new HQLQueryBuilder<E>(
                getEntityManager(), entityClass);
        appendHibernateFilters(queryBuilder);
        List<Object> listWithStatisticsItems = queryBuilder
                .getListWithStatisticsItems(statisticItems, item,
                        statisticItemIndex);
        return listWithStatisticsItems;
    }

    private void refreshListWithStatistics() {
        if (refreshListStatistics) {
            HQLQueryBuilder<E> queryBuilder = new HQLQueryBuilder<E>(
                    getEntityManager(), entityClass);
            appendHibernateFilters(queryBuilder);
            listWithStatistics = queryBuilder
                    .getListWithStatistics(statisticItems);
            refreshListStatistics = false;
        }
    }

    public Map<String, List<Object>> getPossibleValues() {
        return possibleValuesWrapper;
    }

    public List<Object> getPossibleValues(String keyword) {
        refreshPossibleValues(keyword);
        return possibleValues.get(new Keyword(keyword));
    }

    public List<Integer> getPossibleValuesCount(String keyword) {
        if (!isCountEnabled()) {
            return null;
        }
        refreshPossibleValues(keyword);
        return possibleValuesCount.get(new Keyword(keyword));
    }

    protected Session getSession() {
        if (getEntityManager() != null) {
            Object delegate = getEntityManager().getDelegate();
            if (delegate instanceof Session) {
                return (Session) delegate;
            }
        }
        return null;
    }

    public Map<String, Object> getStringValues() {
        refreshStringValues();
        return stringValues;
    }

    public Map<String, Suggester<E, Object>> getSuggesters() {
        return suggesters;
    }

    @Override
    public void modified() {
        markRefresh();
        resetPossibleValues();
        if (filterDataModel != null) {
            filterDataModel.resetCache();
        }
        if (listener != null) {
            listener.filterModified();
        }
    }

    public void markRefresh() {
        refreshStringValues = true;
        refreshListStatistics = true;
    }

    public void resetPossibleValues() {
        possibleValues = new WeakHashMap<Keyword, List<Object>>();
        possibleValuesCount = new WeakHashMap<Keyword, List<Integer>>();
    }

    @SuppressWarnings("unchecked")
    private void refreshPossibleValues(String keyword) {
        Keyword key = new Keyword(keyword);
        if (possibleValues.get(key) == null) {

            HQLCriterion<E, ?> effectiveFilter = criterions.get(keyword);
            if (effectiveFilter.hasPossibleValues()) {

                String path = effectiveFilter.getPath();

                HQLQueryBuilder<E> queryBuilder = null;

                if ((isPossibleFiltered() && effectiveFilter
                        .isPossibleFiltered())
                        || (isCountEnabled() && effectiveFilter
                        .isCountEnabled())) {
                    queryBuilder = new HQLQueryBuilder<E>(getEntityManager(),
                            entityClass);
                    effectiveFilter.appendDefaultFilter(queryBuilder);

                    if (filterDataModel != null) {
                        filterDataModel.appendFiltersFields(queryBuilder);
                    }
                    if (filterValues.get(keyword) == null) {
                        appendHibernateFilters(queryBuilder, null, keyword);
                    } else {
                        appendHibernateFilters(queryBuilder, keyword, keyword);
                    }
                }

                List<Object> items = null;
                if (isPossibleFiltered()
                        && effectiveFilter.isPossibleFiltered()) {
                    items = (List<Object>) queryBuilder.getListDistinct(path);
                } else {
                    HQLQueryBuilder<?> queryBuilderCriterion = new HQLQueryBuilder(
                            getEntityManager(),
                            effectiveFilter.getSelectableClass());
                    items = (List<Object>) queryBuilderCriterion.getList();
                }

                Map<Object, Number> counters = null;
                if (isCountEnabled() && effectiveFilter.isCountEnabled()) {
                    counters = new HashMap<Object, Number>();
                    List<Object[]> statistics = queryBuilder
                            .getStatistics(path);
                    for (Object[] result : statistics) {
                        Object value = result[0];

                        if (value == null) {
                            if (!items.contains(null)) {
                                items.add(null);
                            }
                            value = NullValue.NULL_VALUE;
                        }
                        Number count = (Number) result[1];
                        counters.put(value, count);
                    }
                } else {
                    counters = Collections.EMPTY_MAP;
                }

                HQLCriterion<E, Object> effectiveFilterObject = (HQLCriterion<E, Object>) effectiveFilter;
                Collections.sort(items, new ValueComparator(
                        effectiveFilterObject));

                List<Object> listValues = new ArrayList<Object>();
                List<Integer> listCount = new ArrayList<Integer>();
                for (Object e : items) {
                    Object realValue = e;
                    if (e == null) {
                        realValue = NullValue.NULL_VALUE;
                    }
                    listValues.add(realValue);
                    Number counter = counters.get(realValue);
                    if (counter != null) {
                        listCount.add(counter.intValue());
                    } else {
                        listCount.add(-1);
                    }
                }

                possibleValuesCount.put(key, listCount);
                possibleValues.put(key, listValues);
            }
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void refreshStringValues() {
        if (refreshStringValues) {
            stringValues = new HashMap<String, Object>();
            for (HQLCriterion effectiveFilter : criterions.values()) {
                String keyword = effectiveFilter.getKeyword();
                Object value = filterValues.get(keyword);
                if (value != null) {
                    String selectableLabel = formatters.get(keyword)
                            .formatLabel(value);
                    stringValues.put(keyword, selectableLabel);
                }
            }
            refreshStringValues = false;
        }
    }

    public void resetField(String fieldId) {
        filterValues.remove(fieldId);
        if (stringValues != null) {
            stringValues.remove(fieldId);
        }
    }

    public void resetDateField(String fieldId) {
        DateValue dateValue = (DateValue) filterValues.remove(fieldId);
        dateValue.setIntervalType(IntervalType.ANY);
        filterValues.put(fieldId, dateValue);
    }

    public void resetNumberField(String fieldId) {
        NumberValue nbValue = (NumberValue) filterValues.remove(fieldId);
        nbValue.setIntervalType(IntervalType.ANY);
        filterValues.put(fieldId, nbValue);
    }

    public void setFilterDataModel(FilterDataModel<E> filterDataModel) {
        this.filterDataModel = filterDataModel;
    }

    public void setFilterValues(MapNotifier filterValues) {
        this.filterValues = filterValues;
        this.filterValues.addListener(this);
    }

    public void setStatisticItems(List<HQLStatisticItem> statisticItems) {
        this.statisticItems = statisticItems;
    }

    public void setUpdatedField(String updatedField) {
        this.updatedField = updatedField;
        resetPossibleValues();
        if (criterions.get(updatedField) instanceof DateCriterion<?>) {
            modified();
        }
    }

    public String getUpdatedField() {
        return updatedField;
    }

    public boolean isCountEnabled() {
        return true && isPossibleFiltered();
    }

    public boolean isPossibleFiltered() {
        return true;
    }

    private void initFromUrl(Map<String, String> requestParameterMap) {
        DatatableStateHolderBean dataTableState = null;

        Object dataTableStateObject = Component
                .getInstance("dataTableStateHolder");
        if (dataTableStateObject instanceof DatatableStateHolderBean) {
            dataTableState = (DatatableStateHolderBean) dataTableStateObject;
        }

        Set<Entry<String, String>> entrySet = requestParameterMap.entrySet();
        for (Entry<String, String> entry : entrySet) {
            String stringKey = entry.getKey();
            String stringValue = entry.getValue();
            if (stringKey.startsWith("filter.") && (dataTableState != null)) {
                stringKey = StringUtils.replaceOnce(stringKey, "filter.", "");
                dataTableState.getColumnFilterValues().put(stringKey,
                        stringValue);
            } else if (stringKey.startsWith("sort.")
                    && (dataTableState != null)) {
                stringKey = StringUtils.replaceOnce(stringKey, "sort.", "");
                SortOrder orderingValue = SortOrder.valueOf(stringValue);
                dataTableState.getSortOrders().put(stringKey, orderingValue);
            } else {
                Converter converter = getConverter(stringKey);
                if (converter != null) {
                    if (stringValue.equals("null")) {
                        filterValues.put(stringKey, NullValue.NULL_VALUE);
                    } else {
                        Object realValue = converter.getAsObject(
                                FacesContext.getCurrentInstance(),
                                new ComponentRef(), stringValue);
                        if (realValue == null) {
                            try {
                                Integer id = Integer.valueOf(stringValue);
                                HQLCriterion<E, ?> abstractCriterion = criterions
                                        .get(stringKey);
                                Class<?> selectableClass = abstractCriterion
                                        .getSelectableClass();

                                realValue = getEntityManager().find(
                                        selectableClass, id);
                            } catch (NumberFormatException e) {
                                realValue = null;
                            }
                        }
                        filterValues.put(stringKey, realValue);
                    }
                }
            }
        }

        modified();
    }

    public void setUrlParameters(String urlParameters) {
        //
    }

    public String getUrlParameters() {
        StringBuilder sb = new StringBuilder();

        for (HQLCriterion<E, ?> effectiveFilter : criterions.values()) {
            String keyword = effectiveFilter.getKeyword();
            Object value = filterValues.get(keyword);
            if (value != null) {
                String id = null;
                if (NullValue.NULL_VALUE.equals(value)) {
                    id = "null";
                } else {
                    try {
                        id = PropertyUtils.getProperty(value, "id").toString();
                    } catch (Throwable e) {
                        Converter converter = getConverter(keyword);
                        if (converter != null) {
                            id = converter.getAsString(
                                    FacesContext.getCurrentInstance(),
                                    new ComponentRef(), value);
                        } else {
                            id = value.toString();
                        }
                    }
                }
                if (sb.length() != 0) {
                    sb.append('&');
                }

                try {
                    String idEncoded = URLEncoder.encode(id, StandardCharsets.UTF_8.toString());
                    sb.append(keyword).append('=').append(idEncoded);
                } catch (UnsupportedEncodingException e) {
                    // Do not append the parameter
                }
            }
        }

        Object dataTableStateObject = Component
                .getInstance("dataTableStateHolder");
        if (dataTableStateObject instanceof DatatableStateHolderBean) {
            DatatableStateHolderBean dataTableState = (DatatableStateHolderBean) dataTableStateObject;

            Map<String, Object> columnFilterValues = dataTableState
                    .getColumnFilterValues();
            Set<Entry<String, Object>> filterEntrySet = columnFilterValues
                    .entrySet();
            for (Entry<String, Object> entry : filterEntrySet) {
                if (entry.getValue() != null) {
                    if (sb.length() != 0) {
                        sb.append('&');
                    }
                    sb.append("filter.").append(entry.getKey()).append('=')
                            .append(entry.getValue());
                }
            }

            Map<String, SortOrder> sortOrders = dataTableState.getSortOrders();
            Set<Entry<String, SortOrder>> sortEntrySet = sortOrders.entrySet();
            for (Entry<String, SortOrder> entry : sortEntrySet) {
                if (entry.getValue() != SortOrder.unsorted) {
                    if (sb.length() != 0) {
                        sb.append('&');
                    }
                    sb.append("sort.").append(entry.getKey()).append('=')
                            .append(entry.getValue());
                }
            }
        }

        return sb.toString();
    }

    public Object getRealFilterValue(String filterId) {
        Object filterValue = filterValues.get(filterId);
        if ((filterValue == null) || NullValue.NULL_VALUE.equals(filterValue)) {
            return null;
        }
        HQLCriterion<E, ?> criterion = criterions.get(filterId);
        if (criterion instanceof PropertyCriterion) {
            PropertyCriterion propertyCriterion = (PropertyCriterion) criterion;
            Class realSelectableClass = propertyCriterion
                    .getRealSelectableClass();
            if (!realSelectableClass.equals(propertyCriterion
                    .getSelectableClass())) {
                filterValue = propertyCriterion.getRealValue(filterValue);
            }
        }
        return filterValue;
    }

    public List<SelectItem> getIntervalTypes() {
        List<SelectItem> result = new ArrayList<SelectItem>();
        result.add(new SelectItem(IntervalType.ANY, ResourceBundle.instance().getString("gazelle.common.Any")));
        result.add(new SelectItem(IntervalType.BEFORE, ResourceBundle.instance().getString("gazelle.common.Before")));
        result.add(new SelectItem(IntervalType.AFTER, ResourceBundle.instance().getString("gazelle.common.After")));
        result.add(new SelectItem(IntervalType.BETWEEN, ResourceBundle.instance().getString("gazelle.common.Between")));
        return result;
    }
}
