package net.ihe.gazelle.common.servletfilter;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.services.GenericServiceLoader;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.Filter;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.web.AbstractFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 */
@Scope(ScopeType.APPLICATION)
@Name("net.ihe.gazelle.common.servletfilter.CSPHeader")
@Install
@BypassInterceptors
@Filter
public class CSPHeaderFilter extends AbstractFilter {

    /**
     * Field httpSecurityPolicies.
     */
    private static Map<String, String> httpSecurityPolicies;

    private static Boolean httpSecurityPoliciesEnable;

    /**
     * Method clearCache.
     * <p/>
     * Clear cached policies
     */
    public static void clearCache() {
        httpSecurityPolicies = null;
    }

    /**
     * Method doFilter.
     * <p/>
     * Add http security headers to .seam content sent to the client
     *
     * @param request  ServletRequest
     * @param response ServletResponse
     * @param chain    FilterChain
     *
     * @throws IOException
     * @throws ServletException
     * @see javax.servlet.Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        if (response instanceof HttpServletResponse) {
            final HttpServletRequest httpServletRequest = (HttpServletRequest) request;

            if (isThisASeamPageRequested(httpServletRequest)) {
                applyHttpHeaderPolicy(response, httpServletRequest);
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * Method applyHttpHeaderPolicy.
     *
     * @param response           ServletResponse
     * @param httpServletRequest HttpServletRequest
     */
    private void applyHttpHeaderPolicy(ServletResponse response, final HttpServletRequest httpServletRequest) {
        loadHttpHeaderPolicy();
        if (isSecurityPolicyEnabled()) {

            final HttpServletResponse httpServletResponse = (HttpServletResponse) response;

            addHttpHeaderTo(httpServletResponse);
        }
    }

    /**
     * Method loadHttpHeaderPolicy.
     */
    private void loadHttpHeaderPolicy() {
        if (httpSecurityPolicies == null) {
            loadHttpPoliciesFromCache();
        }
    }

    private void httpSecurityPoliciesEnable() {
        if (httpSecurityPoliciesEnable == null) {
            loadHttpSecurityPoliciesEnable();
        }
    }

    /**
     * Method addHttpHeaderTo.
     *
     * @param httpServletResponse HttpServletResponse
     */
    private void addHttpHeaderTo(final HttpServletResponse httpServletResponse) {
        if (isSecurityPolicyEnabled()) {
            for (Map.Entry<String, String> httpSecurityPolicy : httpSecurityPolicies.entrySet()) {
                httpServletResponse.setHeader(httpSecurityPolicy.getKey(), httpSecurityPolicy.getValue());
            }
        }
    }

    /**
     * Method isSecurityPolicyEnabled.
     *
     * @return boolean
     */
    private boolean isSecurityPolicyEnabled() {
        if (httpSecurityPoliciesEnable == null) {
            return false;
        } else {
            return httpSecurityPoliciesEnable;
        }
    }

    /**
     * Method isThisASeamPageRequested.
     *
     * @param httpServletRequest HttpServletRequest
     *
     * @return boolean
     */
    private boolean isThisASeamPageRequested(final HttpServletRequest httpServletRequest) {
        return httpServletRequest.getRequestURI().endsWith(".seam");
    }

    /**
     * Method loadHttpPolicies.
     */
    private static void loadHttpPoliciesFromCache() {
        final boolean createContexts = !Contexts.isEventContextActive()
                && !Contexts.isApplicationContextActive();
        Map<String, String> httpSecurityPoliciesFromDb = null;
        if (createContexts) {
            Lifecycle.beginCall();
        }
        final CSPPoliciesPreferences applicationPreferenceManager = GenericServiceLoader
                .getService(CSPPoliciesPreferences.class);
        if (applicationPreferenceManager != null) {
            httpSecurityPoliciesFromDb = applicationPreferenceManager.getHttpSecurityPolicies();
            httpSecurityPoliciesEnable = applicationPreferenceManager.isContentPolicyActivated();
        }
        if (httpSecurityPoliciesFromDb == null) {
            httpSecurityPoliciesFromDb = setDefaultSecurityPolicies();
            httpSecurityPoliciesEnable = false;
        }

        if (createContexts) {
            Lifecycle.endCall();
        }
        httpSecurityPolicies = httpSecurityPoliciesFromDb;
    }

    private void loadHttpSecurityPoliciesEnable() {

    }

    private static Map<String, String> setDefaultSecurityPolicies() {
        final Map<String, String> defaultHttpSecurityPolicies = new HashMap<String, String>();
        defaultHttpSecurityPolicies.put(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(PreferencesKey.CACHE_CONTROL.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(
                PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_CHROME.getFriendlyName(), "");
        defaultHttpSecurityPolicies.put(
                PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY_CHROME.getFriendlyName(), "");
        return defaultHttpSecurityPolicies;
    }
}