package net.ihe.gazelle.common.util;

import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.international.LocaleSelector;

import javax.faces.model.SelectItem;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateDisplayUtil {

    private static final String TIMEZONE_ID_PREFIXES = "^(Africa|America|Asia|Atlantic|Australia|Europe|Indian|Pacific)/.*";
    private static final String TIMEZONE_ID_PREFIXES_FOR_OFFSETS = "^(Africa|America|Asia|Atlantic|Australia|Europe|Etc|Indian|Pacific)/.*";

    private static List<TimeZone> TIME_ZONE_LIST = null;
    private static List<TimeZone> TIME_ZONE_LIST_OFFSETS = null;

    private static final Map<MultiKey, String> TIME_ZONE_TRANSLATIONS = Collections
            .synchronizedMap(new HashMap<MultiKey, String>());
    private static final Map<MultiKey, DateFormat> TIME_ZONE_FORMATTERS = Collections
            .synchronizedMap(new HashMap<MultiKey, DateFormat>());

    private DateDisplayUtil() {
    }

    private static List<TimeZone> getTimeZoneList() {
        if (TIME_ZONE_LIST == null) {
            synchronized (DateDisplayUtil.class) {
                if (TIME_ZONE_LIST == null) {
                    TIME_ZONE_LIST = getTimeZoneListCompute();
                }
            }
        }
        return TIME_ZONE_LIST;
    }

    private static List<TimeZone> getTimeZoneListForOffsets() {
        if (TIME_ZONE_LIST_OFFSETS == null) {
            synchronized (DateDisplayUtil.class) {
                if (TIME_ZONE_LIST_OFFSETS == null) {
                    TIME_ZONE_LIST_OFFSETS = getTimeZoneListComputeForOffsets();
                }
            }
        }
        return TIME_ZONE_LIST_OFFSETS;
    }

    private static List<TimeZone> getTimeZoneListComputeForOffsets() {
        List<TimeZone> timeZonesList = new ArrayList<TimeZone>();
        String[] timeZoneIds = TimeZone.getAvailableIDs();
        for (final String id : timeZoneIds) {
            if (id.matches(TIMEZONE_ID_PREFIXES_FOR_OFFSETS)) {
                timeZonesList.add(TimeZone.getTimeZone(id));
            }
        }
        Collections.sort(timeZonesList, new Comparator<TimeZone>() {
            @Override
            public int compare(final TimeZone a, final TimeZone b) {
                String aId = a.getID();
                String bId = b.getID();
                if (aId.startsWith("Etc/") && bId.startsWith("Etc/")) {
                    return aId.compareTo(bId);
                }
                if (aId.startsWith("Etc/")) {
                    return -1;
                }
                if (bId.startsWith("Etc/")) {
                    return 1;
                }
                return aId.compareTo(bId);
            }
        });
        return timeZonesList;
    }

    private static List<TimeZone> getTimeZoneListCompute() {
        List<TimeZone> timeZonesList = new ArrayList<TimeZone>();
        String[] timeZoneIds = TimeZone.getAvailableIDs();
        for (final String id : timeZoneIds) {
            if (id.matches(TIMEZONE_ID_PREFIXES)) {
                timeZonesList.add(TimeZone.getTimeZone(id));
            }
        }
        Collections.sort(timeZonesList, new Comparator<TimeZone>() {
            @Override
            public int compare(final TimeZone a, final TimeZone b) {
                return a.getID().compareTo(b.getID());
            }
        });
        return timeZonesList;
    }

    public static String guessTimeZone(int timezoneOffset) {
        long date = new Date().getTime();
        for (TimeZone timeZone : getTimeZoneListForOffsets()) {
            int timeZoneTestOffset = ((timeZone.getOffset(date) / 1000) / 60);
            if (timeZoneTestOffset == timezoneOffset) {
                return timeZone.getID();
            }
        }
        return null;
    }

    public static String getTimeZoneAsString(TimeZone timeZone) {
        return getTimeZoneAsString(timeZone, new Date());
    }

    public static String getTimeZoneAsString(TimeZone timeZone, Date date) {
        if (date == null) {
            return getTimeZoneAsString(timeZone, new Date());
        }
        Locale locale = LocaleSelector.instance().getLocale();
        MultiKey key = new MultiKey(timeZone.getID(), locale);
        String result = TIME_ZONE_TRANSLATIONS.get(key);
        if (result == null) {
            String longDisplayName = timeZone.getDisplayName(false, TimeZone.LONG, locale);
            result = timeZone.getID().replaceAll("_", " ") + " OFFSET - " + longDisplayName;
            TIME_ZONE_TRANSLATIONS.put(key, result);
        }
        return result.replace("OFFSET", getTimeZoneOffset(timeZone, date));
    }

    public static String getTimeZoneOffset(TimeZone timeZone) {
        return getTimeZoneOffset(timeZone, new Date());
    }

    public static String getTimeZoneOffset(TimeZone timeZone, Date date) {
        if (date == null) {
            return getTimeZoneOffset(timeZone, new Date());
        }
        Locale locale = LocaleSelector.instance().getLocale();
        MultiKey key = new MultiKey(timeZone.getID(), locale);
        DateFormat df = TIME_ZONE_FORMATTERS.get(key);
        if (df == null) {
            if (timeZone.getID().startsWith("Etc/")) {
                df = new SimpleDateFormat("z", locale);
            } else {
                df = new SimpleDateFormat("z' GMT'Z", locale);
            }
            df.setTimeZone(timeZone);
            TIME_ZONE_FORMATTERS.put(key, df);
        }
        return df.format(date);
    }

    public static List<SelectItem> getTimeZones() {
        List<SelectItem> timeZoneItems = new ArrayList<SelectItem>();
        timeZoneItems.add(new SelectItem(null, ResourceBundle.instance().getString("gazelle.common.PleaseSelect")));
        for (TimeZone timeZone : getTimeZoneList()) {
            timeZoneItems.add(new SelectItem(timeZone.getID(), getTimeZoneAsString(timeZone)));
        }
        return timeZoneItems;
    }

    public static TimeZone getTimeZone() {
        return getTimeZone(false);
    }

    public static TimeZone getTimeZone(boolean useApplicationTimeZone) {
        TimeZone timeZone = null;
        try {
            String timeZoneId = null;
            if (!useApplicationTimeZone) {
                timeZoneId = PreferenceService.getString("user_time_zone");
            }
            if (StringUtils.trimToNull(timeZoneId) == null) {
                timeZoneId = PreferenceService.getString("time_zone");
            }
            timeZone = TimeZone.getTimeZone(timeZoneId);
        } catch (Exception e) {
            timeZone = TimeZone.getDefault();
        }
        return timeZone;
    }

    private static DateFormat getDateFormat(boolean showDate, boolean showTime, int formatDate, int formatTime,
                                            TimeZone timeZone) {
        Locale locale = LocaleSelector.instance().getLocale();
        DateFormat dateFormatInstance = null;
        if (showDate && showTime) {
            dateFormatInstance = DateFormat.getDateTimeInstance(formatDate, formatTime, locale);
        } else if (showDate) {
            dateFormatInstance = DateFormat.getDateInstance(formatDate, locale);
        } else if (showTime) {
            dateFormatInstance = DateFormat.getTimeInstance(formatTime, locale);
        }
        dateFormatInstance.setTimeZone(timeZone);
        return dateFormatInstance;
    }

    private static String displayDate(Date date, boolean showDate, boolean showTime, int formatDate, int formatTime,
                                      boolean useApplicationTimeZone, boolean htmlOutput) {

        if (date == null) {
            return "";
        }

        TimeZone timeZone;
        timeZone = getTimeZone(useApplicationTimeZone);

        DateFormat dateFormatInstance = getDateFormat(showDate, showTime, formatDate, formatTime, timeZone);
        if (showTime) {
            if (htmlOutput) {
                StringBuilder sb = new StringBuilder();
                sb.append("<span style=\"white-space: nowrap;\" data-tooltip=\"");
                sb.append(StringEscapeUtils.escapeJava(getTimeZoneOffset(timeZone, date)));
                sb.append("\">");
                sb.append(StringEscapeUtils.escapeHtml(dateFormatInstance.format(date)));
                sb.append("</span>");
                return sb.toString();
            } else {
                return dateFormatInstance.format(date) + " (" + getTimeZoneOffset(timeZone, date) + ")";
            }
        } else {
            return dateFormatInstance.format(date);
        }
    }

    private static String displayDate(Date date, boolean showDate, boolean showTime, boolean useApplicationTimeZone) {
        return displayDate(date, showDate, showTime, DateFormat.SHORT, DateFormat.MEDIUM, useApplicationTimeZone, false);
    }

    public static String displayDate(Date date) {
        // use user time zone if possible
        return displayDate(date, false);
    }

    public static String displayDate(Date date, boolean useApplicationTimeZone) {
        return displayDate(date, true, false, useApplicationTimeZone);
    }

    public static String displayTime(Date date, boolean useApplicationTimeZone) {
        return displayDate(date, false, true, useApplicationTimeZone);
    }

    public static String displayTime(Date date) {
        // use user time zone if possible
        return displayTime(date, false);
    }

    public static String displayDateTime(Date date, boolean useApplicationTimeZone) {
        return displayDate(date, true, true, useApplicationTimeZone);
    }

    public static String displayDateTime(Date date) {
        // use user time zone if possible
        return displayDateTime(date, false);
    }

    public static String displayDateTimeHTML(Date date) {
        return displayDate(date, true, true, DateFormat.SHORT, DateFormat.MEDIUM, false, true);
    }

    public static String getCalendarPatternDateTime() {
        DateFormat df = getDateFormat(true, true, DateFormat.SHORT, DateFormat.SHORT, getCalendarTimeZoneDateTime());
        if (df instanceof SimpleDateFormat) {
            SimpleDateFormat sdf = (SimpleDateFormat) df;
            return sdf.toPattern();
        } else {
            return "d/M/yy HH:mm";
        }
    }

    public static String getCalendarPatternDate() {
        DateFormat df = getDateFormat(true, false, DateFormat.SHORT, DateFormat.SHORT, getCalendarTimeZoneDate());
        if (df instanceof SimpleDateFormat) {
            SimpleDateFormat sdf = (SimpleDateFormat) df;
            return sdf.toPattern();
        } else {
            return "d/M/yy";
        }
    }

    public static Locale getCalendarLocale() {
        return LocaleSelector.instance().getLocale();
    }

    public static TimeZone getCalendarTimeZoneDateTime() {
        return getTimeZone();
    }

    public static TimeZone getCalendarTimeZoneDate() {
        return getTimeZone();
    }
}
