package net.ihe.gazelle.common.bootstrapComponent;

import org.richfaces.renderkit.MessageForRender;
import org.richfaces.renderkit.MessageRendererBase;
import org.richfaces.renderkit.RenderKitUtils;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;

import static org.richfaces.renderkit.RenderKitUtils.attributes;
import static org.richfaces.renderkit.RenderKitUtils.renderPassThroughAttributes;


@ResourceDependencies({@ResourceDependency(name = "jsf.js", library = "javax.faces", target = ""), @ResourceDependency(name = "jquery.js", library = "", target = ""), @ResourceDependency(name = "richfaces.js", library = "", target = ""), @ResourceDependency(name = "richfaces-base-component.js", library = "", target = ""), @ResourceDependency(name = "richfaces-event.js", library = "", target = ""), @ResourceDependency(name = "message.js", library = "org.richfaces", target = ""), @ResourceDependency(name = "msg.ecss", library = "org.richfaces", target = "")})
public class HtmlMessagesRenderer extends MessageRendererBase {

    private static final RenderKitUtils.Attributes PASS_THROUGH_ATTRIBUTES4 = attributes()
            .generic("dir", "dir")


            .generic("lang", "lang")


            .generic("onclick", "onclick", "click")


            .generic("ondblclick", "ondblclick", "dblclick")


            .generic("onkeydown", "onkeydown", "keydown")


            .generic("onkeypress", "onkeypress", "keypress")


            .generic("onkeyup", "onkeyup", "keyup")


            .generic("onmousedown", "onmousedown", "mousedown")


            .generic("onmousemove", "onmousemove", "mousemove")


            .generic("onmouseout", "onmouseout", "mouseout")


            .generic("onmouseover", "onmouseover", "mouseover")


            .generic("onmouseup", "onmouseup", "mouseup")


            .generic("role", "role")


            .generic("style", "style")


            .generic("title", "title");


    private static String convertToString(Object object) {
        return object != null ? object.toString() : "";
    }

    @Override
    public void doEncodeEnd(ResponseWriter responseWriter, FacesContext facesContext, UIComponent component)
            throws IOException {

        String clientId = component.getClientId(facesContext);
        responseWriter.startElement("span", component);
        {
            String value = clientId;
            if (null != value &&
                    value.length() > 0
                    ) {
                responseWriter.writeAttribute("id", value, null);
            }

        }
        renderPassThroughAttributes(facesContext, component,
                PASS_THROUGH_ATTRIBUTES4);

        for (MessageForRender msg : this.getVisibleMessages(facesContext, component)) {
            String msgStyle = this.getMsgClass(facesContext, component, msg) + " alert-dismissible " + convertToString(component.getAttributes().get("styleClass"));

            responseWriter.startElement("div", null);
            responseWriter.writeAttribute("class", msgStyle, null);
            responseWriter.writeAttribute("role", "alert", null);

            responseWriter.startElement("button", null);
            responseWriter.writeAttribute("type", "button", null);
            responseWriter.writeAttribute("class", "close", null);
            responseWriter.writeAttribute("data-dismiss", "alert", null);
            responseWriter.writeAttribute("aria-label", "Close", null);

            responseWriter.startElement("span", null);
            responseWriter.writeAttribute("aria-hidden", "true", null);
            responseWriter.write("&times;");
            responseWriter.endElement("span");

            responseWriter.endElement("button");

            encodeMessage(facesContext, component, msg);
            responseWriter.endElement("div");
        }
        encodeScript(facesContext, component);
        responseWriter.endElement("span");
    }
}

