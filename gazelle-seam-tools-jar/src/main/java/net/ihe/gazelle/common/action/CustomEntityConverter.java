package net.ihe.gazelle.common.action;

import static org.jboss.seam.ScopeType.CONVERSATION;
import static org.jboss.seam.annotations.Install.BUILT_IN;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;

import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.ui.EntityConverter;

@Name("net.ihe.gazelle.common.action.CustomEntityConverter")
@Scope(CONVERSATION)
@Install(precedence = BUILT_IN)
@Converter
@BypassInterceptors
public class CustomEntityConverter extends EntityConverter {

	private static final long serialVersionUID = 3861619310986918190L;

	public static final String NULL_VALUE = "-1";

	@Override
	@Transactional
	public String getAsString(FacesContext facesContext, UIComponent cmp, Object value) throws ConverterException {
		if (value == null) {
			return null;
		}
		if (value instanceof String) {
			return (String) value;
		}
		try {
			return getEntityLoader().put(value);
		} catch (IllegalArgumentException e) {
			return NULL_VALUE;
		}
	}

	@Override
	@Transactional
	public Object getAsObject(FacesContext facesContext, UIComponent cmp, String value) throws ConverterException {
		if ((value == null) || (value.length() == 0)) {
			return null;
		}
		if (NULL_VALUE.equals(value)) {
			return null;
		}
		return getEntityLoader().get(value);
	}
}
