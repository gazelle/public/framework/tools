package net.ihe.gazelle.common.excel;

import com.sun.faces.facelets.compiler.UIInstructions;
import net.ihe.gazelle.common.LinkDataProvider;
import net.ihe.gazelle.common.LinkDataProviderService;
import net.ihe.gazelle.common.filter.HibernateDataModel;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.core.Interpolator;
import org.jboss.seam.framework.Query;
import org.jboss.seam.navigation.Pages;
import org.jboss.seam.ui.component.html.HtmlDecorate;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeTraversor;
import org.richfaces.component.UICollapsibleSubTable;
import org.richfaces.component.UIDataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIColumn;
import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.model.DataModel;
import javax.faces.render.RenderKit;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by jlabbe on 26/02/15.
 */
@Name("gazelleExcelExporterJboss7CollapsibleSubPanel")
@Scope(ScopeType.EVENT)
@Install(precedence = Install.BUILT_IN)
@BypassInterceptors
public class GazelleExcelExporterJboss7CollapsibleSubPanel {

    private static Logger log = LoggerFactory.getLogger(GazelleExcelExporterJboss7.class);
    private boolean printLinks = true;

    public void export(String dataTableId, boolean printLinks) throws IOException {
        this.printLinks = printLinks;
        export(dataTableId);
    }

    public void export(String dataTableId) throws IOException {
        Workbook wb = new HSSFWorkbook();

        // Gets the datatable
        UIDataTable dataTable = (UIDataTable) FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(dataTableId);
        if (dataTable == null) {
            throw new RuntimeException(
                    Interpolator.instance().interpolate("Could not find data table with id #0", dataTableId));
        }

        Sheet sheet = wb.createSheet();
        sheet.createFreezePane(0, 1);

        CellStyle styleEven = wb.createCellStyle();
        styleEven.setBorderBottom(CellStyle.BORDER_DOUBLE);
        styleEven.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styleEven.setBorderTop(CellStyle.BORDER_THIN);
        styleEven.setTopBorderColor(IndexedColors.BLACK.getIndex());
        styleEven.setBorderLeft(CellStyle.BORDER_THIN);
        styleEven.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        styleEven.setBorderRight(CellStyle.BORDER_THIN);
        styleEven.setRightBorderColor(IndexedColors.BLACK.getIndex());

        // Saves the datatable var
        String dataTableVar = dataTable.getVar();
        Object oldValue = FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(dataTableVar);

        List<UIColumn> columns = getChildrenOfType(dataTable.getFacet("header").getChildren(), javax.faces.component.UIColumn.class);

        int rowIndex = 0;
        Row row = sheet.createRow(rowIndex);
        rowIndex++;

        int columnIndex = 0;
        for (UIColumn uiColumn : columns) {
            Cell cell = row.createCell(columnIndex);
            List<UIComponent> dataOutputs = getChildrenOfType(uiColumn.getChildren(), UIComponent.class);
            processOutputs(cell, uiColumn, dataOutputs);
            cell.setCellStyle(styleEven);
            columnIndex++;
        }
        row.setRowStyle(styleEven);

        columns = getChildrenOfType(dataTable.getChildren().get(0).getChildren(), javax.faces.component.UIColumn.class);

        UICollapsibleSubTable subTable;
        Iterator iterator;
        Iterator dataTableIterator = unwrapIterator(dataTable.getValue());

        while (dataTableIterator.hasNext()) {
            Object dataTableRowValue = dataTableIterator.next();
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(dataTable.getVar(), dataTableRowValue);

            UIComponent uiComponent = dataTable.getChildren().get(0);
            subTable = (UICollapsibleSubTable) uiComponent;
            dataTableVar = subTable.getVar();
            iterator = unwrapIterator(subTable.getValue());

            rowIndex = printRow(sheet, styleEven, dataTableVar, columns, rowIndex, iterator);
        }

        columnIndex = 0;
        for (UIColumn uiColumn : columns) {
            sheet.autoSizeColumn(columnIndex);
            columnIndex++;
        }

        CellReference start = new CellReference(0, 0);
        String startString = start.formatAsString();
        CellReference end = new CellReference(rowIndex - 1, columnIndex - 1);
        String endString = end.formatAsString();
        sheet.setAutoFilter(CellRangeAddress.valueOf(startString + ":" + endString));

        // Restores the data table var
        if (oldValue == null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().remove(dataTableVar);
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(dataTableVar, oldValue);
        }

        // Redirects to the generated document
        redirectExport(wb);
    }

    private int printRow(Sheet sheet, CellStyle styleEven, String dataTableVar, List<UIColumn> columns, int rowIndex, Iterator iterator) {
        Row row;
        int columnIndex;
        while (iterator.hasNext()) {
            Object rowValue = iterator.next();

            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(dataTableVar, rowValue);
            row = sheet.createRow(rowIndex);
            rowIndex++;
            columnIndex = 0;
            for (UIColumn uiColumn : columns) {
                Cell cell = row.createCell(columnIndex);
                List<UIComponent> dataOutputs = getChildrenOfType(uiColumn.getChildren(), UIComponent.class);
                processOutputs(cell, uiColumn, dataOutputs);
                columnIndex++;
                if ((rowIndex % 2) == 0) {
                    cell.setCellStyle(styleEven);
                } else {
                    cell.setCellStyle(styleEven);
                }
            }

            if ((rowIndex % 2) == 0) {
                row.setRowStyle(styleEven);
            } else {
                row.setRowStyle(styleEven);
            }
        }
        return rowIndex;
    }

    public static <T> List<T> getChildrenOfType(List<UIComponent> children, Class<T> childType) {
        List<T> matches = new ArrayList<T>();
        for (UIComponent child : children) {
            if (childType.isAssignableFrom(child.getClass())) {
                if (UIComponent.class.isAssignableFrom(child.getClass())) {
                    if (child.isRendered()) {
                        matches.add((T) child);
                    }
                } else {
                    matches.add((T) child);
                }
            }
        }
        return matches;
    }

    private void redirectExport(Workbook wb) throws IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();

        String baseName = Pages.getCurrentBaseName();

        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + baseName + ".xls\"");
        wb.write(servletOutputStream);
        servletOutputStream.flush();
        servletOutputStream.close();
        facesContext.responseComplete();
    }

    /**
     * Processes all output type elements (in column)
     *
     * @param cell
     * @param outputs The list of outputs to process
     */
    private void processOutputs(Cell cell, javax.faces.component.UIColumn column, List<UIComponent> outputs) {
        List<Object> content = new ArrayList<Object>();
        List<String> links = new ArrayList<String>();
        for (UIComponent uiComponent : outputs) {
            if (!checkUIOutput(content, links, uiComponent)) {
                List<String> evaluate = evaluate(uiComponent);
                content.add(evaluate.get(0).trim());
                if (evaluate.size() == 2) {
                    links.add(evaluate.get(1));
                }
            }
        }
        boolean set = false;
        if (content.size() == 1) {
            set = true;
            Object value = content.get(0);
            Class<? extends Object> outputValueClass = value.getClass();
            if (Date.class.isAssignableFrom(outputValueClass)) {
                cell.setCellValue((Date) value);
            } else if (Boolean.class.isAssignableFrom(outputValueClass)) {
                cell.setCellValue((Boolean) value);
            } else if (Calendar.class.isAssignableFrom(outputValueClass)) {
                cell.setCellValue((Calendar) value);
            } else if (Number.class.isAssignableFrom(outputValueClass)) {
                cell.setCellValue(((Number) value).doubleValue());
            } else {
                set = false;
            }
        }
        if (!set) {
            boolean first = true;
            StringBuilder sb = new StringBuilder("");
            for (Object value : content) {
                if (first) {
                    first = false;
                } else {
                    if (value != null) {
                        sb.append(' ');
                    }
                }
                if (value != null) {
                    sb.append(value.toString());
                }
            }
            cell.setCellValue(sb.toString());
        }
        if (links.size() == 1) {
            HSSFHyperlink hssfHyperlink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
            hssfHyperlink.setAddress(links.get(0));
            cell.setHyperlink(hssfHyperlink);
        }
    }

    private boolean checkUIOutput(List<Object> content, List<String> links, UIComponent uiComponent) {
        if (uiComponent instanceof UIOutput) {
            Object value = ((UIOutput) uiComponent).getValue();
            if (value == null) {
                return false;
            } else {
                Class<? extends Object> outputValueClass = value.getClass();
                if (Date.class.isAssignableFrom(outputValueClass)) {
                    content.add(value);
                    return true;
                }
                if (Boolean.class.isAssignableFrom(outputValueClass)) {
                    content.add(value);
                    return true;
                }
                if (Calendar.class.isAssignableFrom(outputValueClass)) {
                    content.add(value);
                    return true;
                }
                if (Number.class.isAssignableFrom(outputValueClass)) {
                    content.add(value);
                    return true;
                }
                LinkDataProvider provider = getProvider(value);
                if (provider != null) {
                    String label = provider.getLabel(value, true);
                    content.add(label);
                    String link = provider.getLink(value);
                    if ((link != null) && link.startsWith("http")) {
                        links.add(link);
                    }
                    return true;
                }
            }
        }
        if (uiComponent instanceof UIInstructions || uiComponent instanceof HtmlDecorate) {
            return true;
        }
        return false;
    }

    protected LinkDataProvider getProvider(Object value) {
        if (value != null) {
            Class<?> valueClass = value.getClass();
            return LinkDataProviderService.getProviderForClass(valueClass);
        }
        return null;
    }

    private List<String> evaluate(UIComponent uiComponent) {
        FacesContext context = FacesContext.getCurrentInstance();

        ResponseWriter originalWriter = context.getResponseWriter();

        StringWriter stringWriter = new StringWriter();
        ResponseWriter writer = createResponseWriter(context, stringWriter);
        context.setResponseWriter(writer);

        try {
            uiComponent.encodeAll(context);
        } catch (IOException e) {
            log.error("Failed to render", e);
            return new ArrayList<String>();
        }

        if (originalWriter != null) {
            context.setResponseWriter(originalWriter);
        }

        List<String> result = new ArrayList<String>();

        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        String baseURL = req.getRequestURL().toString();

        Document doc = Jsoup.parse(stringWriter.toString(), baseURL);

        GazelleNodeVisitor formatter = new GazelleNodeVisitor();
        NodeTraversor traversor = new NodeTraversor(formatter);
        traversor.traverse(doc);
        result.add(formatter.toString());

        if (printLinks) {
            Elements links = doc.select("a[href]");
            if (links.size() == 1) {
                Element link = links.get(0);
                String href = link.attr("href");
                if ((href != null) && !href.startsWith("#") && !href.startsWith("?")) {
                    String absHref = link.attr("abs:href");
                    if ((absHref != null) && absHref.startsWith("http")) {
                        result.add(absHref);
                    }
                }
            }
        }

        return result;
    }

    private ResponseWriter createResponseWriter(FacesContext context, Writer writer) {
        ExternalContext extContext = context.getExternalContext();
        Map<String, Object> requestMap = extContext.getRequestMap();
        String contentType = (String) requestMap.get("facelets.ContentType");
        String encoding = (String) requestMap.get("facelets.Encoding");
        RenderKit renderKit = context.getRenderKit();
        return renderKit.createResponseWriter(writer, contentType, encoding);
    }

    @SuppressWarnings("unchecked")
    private static Iterator unwrapIterator(Object value) {
        if (value instanceof HibernateDataModel) {
            return ((HibernateDataModel) value).getAllItems(FacesContext.getCurrentInstance()).iterator();
        } else if (value instanceof Iterable) {
            return ((Iterable) value).iterator();
        } else if ((value instanceof DataModel) && (((DataModel) value).getWrappedData() instanceof Iterable)) {
            return ((Iterable) ((DataModel) value).getWrappedData()).iterator();
        } else if (value instanceof Map) {
            return ((Map) value).entrySet().iterator();
        } else if (value instanceof Query) {
            return (((Query) value).getResultList()).iterator();
        } else if ((value != null) && value.getClass().isArray()) {
            return arrayAsList(value).iterator();
        } else {
            throw new RuntimeException("A worksheet's value must be an Iterable, DataModel or Query");
        }
    }

    private static List arrayAsList(Object array) {
        if (array.getClass().getComponentType().isPrimitive()) {
            List list = new ArrayList();
            for (int i = 0; i < Array.getLength(array); i++) {
                list.add(Array.get(array, i));
            }
            return list;
        } else {
            return Arrays.asList((Object[]) array);
        }
    }
}
