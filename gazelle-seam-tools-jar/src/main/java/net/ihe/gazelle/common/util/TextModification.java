/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.common.util;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * @author Abderrazek Boufahja / INRIA Rennes IHE development Project
 */
public class TextModification {

    private TextModification() {
    }

    public static String formatString(String fileString) {
        StringBuffer result = new StringBuffer();
        if (fileString != null) {
            String[] list_lign = fileString.split("\n");
            for (String s : list_lign) {
                s = s.replaceAll("\t", "----");
                s = StringEscapeUtils.escapeHtml(s);
                result.append(s + "<br/>");
            }
        }
        return result.toString();
    }

    public static String formatColoredString(String fileString) {
        StringBuffer result = new StringBuffer();
        if (fileString != null) {
            String[] list_lign = fileString.split("\n");
            for (String s : list_lign) {
                s = s.replaceAll("\t", "----");
                s = StringEscapeUtils.escapeHtml(s);
                if ((s.indexOf("Error")) != (-1)) {
                    result.append("<span style=\"color:#aa0000;\">" + s + "</span>" + "<br/>");
                } else if ((s.indexOf("Warning")) != (-1)) {
                    result.append("<span style=\"color:#0000aa;\">" + s + "</span>" + "<br/>");
                } else {
                    result.append(s + "<br/>");
                }
            }
        }
        return result.toString();
    }

    public static String string2bbcode(String res) {
        if (res.contains("Error")) {
            return "[b][red]" + res + "[/red][/b]";
        } else if (res.contains("Warning")) {
            return "[b][blue]" + res + "[/blue][/b]";
        } else if (res.contains("Note")) {
            return "[b][yel]" + res + "[/yel][/b]";
        } else {
            return res;
        }
    }
}
