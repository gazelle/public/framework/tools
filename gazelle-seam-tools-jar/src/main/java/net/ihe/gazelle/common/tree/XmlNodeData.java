package net.ihe.gazelle.common.tree;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class XmlNodeData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8320619478974873168L;

	private String namespace;
	
	private String name = "";
	
	private String text;
	
	private Map<String, Object> attributes = new HashMap<String, Object>();

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttribute(String key, Object value) {
		this.attributes.put(key, value);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text != null ? text : "";
	}
	
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	public String getNamespace() {
		return namespace;
	}
	
	public String toString() {
		return getName() + "{" + attributes.toString() + " <" + getText().trim() + ">" + "}";
	}
}