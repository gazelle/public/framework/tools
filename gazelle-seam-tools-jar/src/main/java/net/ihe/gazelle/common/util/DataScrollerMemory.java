package net.ihe.gazelle.common.util;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.jboss.seam.ScopeType.PAGE;

@Name("dataScrollerMemory")
@Scope(PAGE)
@AutoCreate
public class DataScrollerMemory implements Serializable {

   private static final long serialVersionUID = 7973077771941839585L;

   private static final int CONST_10_PAGES = 10;
   private static final int CONST_20_PAGES = 20;
   private static final int CONST_50_PAGES = 50;
   private static final int CONST_100_PAGES = 100;
   private static final Integer DEFAULT_NUMBER_OF_RESULTS_PER_PAGE = CONST_20_PAGES;

   private static final String NUMBER_OF_ITEMS_PER_PAGE = "NUMBER_OF_ITEMS_PER_PAGE";

   private static final List<Integer> POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE = new ArrayList<Integer>();
   static {
      POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE.add(CONST_10_PAGES);
      POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE.add(CONST_20_PAGES);
      POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE.add(CONST_50_PAGES);
      POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE.add(CONST_100_PAGES);
   }

   private final Map<String, Integer> scrollerPage = new DataScrollerMap();
   private Integer numberOfResultsPerPage = null;

   /**
    * Get the map of the current scroller page of dataTables.
    *
    * Warning: the first page has value '1'.
    *
    * @return a Map where the key is the dataTable Id and the value is the selected page to display.
    */
   public Map<String, Integer> getScrollerPage() {
      return scrollerPage;
   }

   public List<Integer> getAllNumberOfResultsPerPage() {
      return POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE;
   }

   public Integer getNumberOfResultsPerPage() {
      if (numberOfResultsPerPage == null) {
         String cookie = GazelleCookie.getCookie(NUMBER_OF_ITEMS_PER_PAGE);
         if (cookie != null) {
            try {
               numberOfResultsPerPage = Integer.valueOf(cookie);
            } catch (NumberFormatException e) {
               setNumberOfResultsPerPage(DEFAULT_NUMBER_OF_RESULTS_PER_PAGE);
            }
         } else {
            setNumberOfResultsPerPage(DEFAULT_NUMBER_OF_RESULTS_PER_PAGE);
         }
      }
      return numberOfResultsPerPage;
   }

   public void setNumberOfResultsPerPage(Integer nb) {
      numberOfResultsPerPage = nb;
      GazelleCookie.setCookie(NUMBER_OF_ITEMS_PER_PAGE, Integer.toString(numberOfResultsPerPage));
   }

   public static class DataScrollerMap extends HashMap<String, Integer> {

      private static final long serialVersionUID = -2619058410381373556L;

      @Override
      public Integer get(Object key) {
         Integer value = super.get(key);
         return value != null ? value : 1 ;
      }
   }

}
