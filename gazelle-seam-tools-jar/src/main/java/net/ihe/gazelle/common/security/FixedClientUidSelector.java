package net.ihe.gazelle.common.security;

import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ui.ClientUidSelector;
import org.jboss.seam.util.RandomStringUtils;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Name("org.jboss.seam.ui.clientUidSelector")
@Install(precedence = Install.DEPLOYMENT)
public class FixedClientUidSelector extends ClientUidSelector {

    private static final long serialVersionUID = 5237089512441072594L;
    private static final int LENGHT = 50;
    private String clientUid;

    @Create
    public void onCreate() {
        String requestContextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        setCookiePath(requestContextPath);
        setCookieMaxAge(-1);
        setCookieEnabled(true);
        clientUid = getCookieValue();
    }

    public void seed() {
        if (!isSet()) {
            clientUid = RandomStringUtils.random(LENGHT, true, true);
            setCookieValueIfEnabled(clientUid);
        }
    }

    public boolean isSet() {
        return clientUid != null;
    }

    public String getClientUid() {
        return clientUid;
    }

    @Override
    protected String getCookieName() {
        return "javax.faces.ClientToken";
    }

    /**
     * Set the cookie
     */
    @Override
    protected void setCookieValueIfEnabled(String value) {
        FacesContext ctx = FacesContext.getCurrentInstance();

        if (isCookieEnabled() && (ctx != null)) {
            HttpServletResponse response = (HttpServletResponse) ctx.getExternalContext().getResponse();
            Cookie cookie = new Cookie(getCookieName(), value);
            cookie.setMaxAge(getCookieMaxAge());
            cookie.setPath(getCookiePath());
            // Fix for IE7
            cookie.setVersion(1);
            response.addCookie(cookie);
        }
    }
}