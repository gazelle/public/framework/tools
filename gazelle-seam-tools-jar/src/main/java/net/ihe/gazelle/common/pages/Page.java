package net.ihe.gazelle.common.pages;

public interface Page {

    String getId();

    String getLabel();

    /**
     * @return a url that ends with .xhtml
     */
    String getLink();

    String getIcon();

    Authorization[] getAuthorizations();

    /**
     * @return a url that ends with .seam to be used in html navigation
     */
    String getMenuLink();
}
