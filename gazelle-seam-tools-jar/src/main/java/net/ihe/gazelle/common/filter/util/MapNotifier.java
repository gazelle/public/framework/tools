/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.filter.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;

public class MapNotifier<K,V> implements Map<K,V>, Serializable {

	private static final long serialVersionUID = -4395625575463186569L;
	private Map<K,V> delegator;
	private List<MapNotifierListener> listeners;

	public MapNotifier() {
		super();
	}

	public MapNotifier(Map<K,V> filterValues) {
		super();
		this.delegator = filterValues;
		this.listeners = new ArrayList<MapNotifierListener>();
	}

	public void addListener(MapNotifierListener listener) {
		listeners.add(listener);
	}

	protected void modified() {
		for (MapNotifierListener listener : listeners) {
			listener.modified();
		}
	}

	@Override
	public int size() {
		return delegator.size();
	}

	@Override
	public boolean isEmpty() {
		return delegator.isEmpty();
	}

	@Override
	public V get(Object key) {
		return delegator.get(key);
	}

	@Override
	public boolean equals(java.lang.Object o) {
		return delegator.equals(o);
	}

	@Override
	public boolean containsKey(java.lang.Object key) {
		return delegator.containsKey(key);
	}

	@Override
	public V put(K key, V value) {
		V result = delegator.put(key, value);
		if (!new EqualsBuilder().append(result, value).isEquals()) {
			modified();
		}
		return result;
	}

	@Override
	public int hashCode() {
		return delegator.hashCode();
	}

	@Override
	public java.lang.String toString() {
		return delegator.toString();
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		delegator.putAll(m);
		modified();
	}

	@Override
	public V remove(java.lang.Object key) {
		V result = delegator.remove(key);
		modified();
		return result;
	}

	@Override
	public void clear() {
		delegator.clear();
		modified();
	}

	@Override
	public boolean containsValue(java.lang.Object value) {
		return delegator.containsValue(value);
	}

	@Override
	public Set<K> keySet() {
		return delegator.keySet();
	}

	@Override
	public Collection<V> values() {
		return delegator.values();
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return delegator.entrySet();
	}

}
