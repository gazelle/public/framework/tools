package net.ihe.gazelle.common.jsf;

/**
 * Object that can have a human-readable label for GUI component (drop-down list, pick list, etc...)
 *
 * @author ceoche
 */
public interface Labelable {

    /**
     * Display a humain-readable label for JSF components
     *
     * @return the label's object
     */
    String getDisplayLabel();

}
