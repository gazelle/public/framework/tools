package net.ihe.gazelle.common.pages;

public interface GazelleSecurityCheckLocal {

	boolean checkSecurity();

}
