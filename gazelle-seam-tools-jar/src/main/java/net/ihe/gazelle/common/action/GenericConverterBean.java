package net.ihe.gazelle.common.action;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metadata.ClassMetadata;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.core.Init;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Map;

@Name("genericConverter")
@AutoCreate
@Scope(ScopeType.APPLICATION)
public class GenericConverterBean implements GenericConverterLocal {

    @Override
    @Observer("org.jboss.seam.postInitialization")
    public void registerConverters() {

        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        Object delegate = entityManager.getDelegate();

        SessionFactoryImplementor factory = null;

        if (delegate instanceof Session) {
            Session session = (Session) delegate;
            SessionFactory sessionFactory = session.getSessionFactory();
            if (sessionFactory instanceof SessionFactoryImplementor) {
                factory = (SessionFactoryImplementor) sessionFactory;
            }
        }
        if (factory == null) {
            throw new IllegalArgumentException();
        }

        Map<String, ClassMetadata> allClassMetadata = factory.getAllClassMetadata();

        Collection<ClassMetadata> values = allClassMetadata.values();
        for (ClassMetadata classMetadata : values) {
            Class<?> mappedClass = classMetadata.getMappedClass();
            Init.instance().getConvertersByClass()
                    .put(mappedClass, "net.ihe.gazelle.common.action.CustomEntityConverter");
        }
    }

    @Override
    @Create
    public void init() {
        // ?
    }

    @Override
    @Destroy
    //@Remove
    public void stopGenericConverter() {
        // ?
    }
}
