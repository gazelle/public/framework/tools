package net.ihe.gazelle.common.action;

import javax.ejb.Stateless;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.Name;

@Stateless

@Name("PUProvider")
public class PUProvider {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Factory(scope=ScopeType.APPLICATION, value="entityManagerFactory")
    public EntityManagerFactory getFactory() {return entityManagerFactory;}

}