package net.ihe.gazelle.common.filecache;

import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Provides a file cache, by rendering it when result misses
 */
public final class FileCache {
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(FileCache.class);
    private static final int PADDING_SIZE = 8;

    private FileCache() {
        // private
    }

    public static void getFile(String key, String value, FileCacheRenderer renderer) throws Exception {

        // Retrieve root catalina response (for sending file on the unfiltered
        // ouput stream)
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse catalinaResponse = (HttpServletResponse) externalContext.getResponse();
        HttpServletRequest catalinaRequest = (HttpServletRequest) externalContext.getRequest();
        try {
            while (true) {
                catalinaResponse = (HttpServletResponse) PropertyUtils.getProperty(catalinaResponse, "response");
            }
        } catch (NoSuchMethodException e1) {
            // nothing to do
            // we are at org.apache.catalina.connector.ResponseFacade
        }
        try {
            while (true) {
                catalinaRequest = (HttpServletRequest) PropertyUtils.getProperty(catalinaResponse, "request");
            }
        } catch (NoSuchMethodException e1) {
            // nothing to do
            // we are at org.apache.catalina.connector.ResponseFacade
        }
        getFile(catalinaRequest, catalinaResponse, key, value, renderer);

        facesContext.responseComplete();
    }

    public static void getFile(HttpServletRequest catalinaRequest, HttpServletResponse catalinaResponse, String key,
                               String value, FileCacheRenderer renderer) throws Exception, IOException {
        catalinaResponse.reset();

        // retrieve rendered file
        File cachedFile = fill(key, value, renderer);
        String currentETag = getETag(cachedFile);

        catalinaResponse.setContentType(renderer.getContentType());
        catalinaResponse.setHeader("ETag", currentETag);

        String eTag = catalinaRequest.getHeader("If-None-Match");
        if (currentETag.equals(eTag)) {
            catalinaResponse.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
        } else {
            // send new version
            ServletOutputStream out = catalinaResponse.getOutputStream();
            // Send value to OutputStream
            FileUtils.copyFile(cachedFile, out);
            out.flush();
            out.close();
        }
    }

    private static String getETag(File cachedFile) {
        return Long.toHexString(cachedFile.lastModified());
    }

    public static void resetCache(String key) {
        String cachedFilePath = getCachedFilePath(key);
        File cachedFile = new File(cachedFilePath);
        if (cachedFile.exists()) {
            cachedFile.delete();
        }
    }

    public static void forceCache(String key, String value, FileCacheRenderer renderer) throws Exception {
        String cachedFilePath = getCachedFilePath(key);
        File cachedFile = new File(cachedFilePath);
        File cachedFileValue = new File(cachedFilePath + ".value");

        deleteCache(cachedFile, cachedFileValue);

        try {
            // Save value
            FileUtils.writeStringToFile(cachedFileValue, value);

            // Save content to cache
            FileOutputStream fos = new FileOutputStream(cachedFile);
            renderer.render(fos, value);
            fos.close();
        } catch (IOException e) {
            // Failed to write cache...
            deleteCache(cachedFile, cachedFileValue);
            throw new Exception("Failed to render file", e);
        }
    }

    private static File fill(String key, String value, FileCacheRenderer renderer) throws Exception {
        String cachedFilePath = getCachedFilePath(key);
        File cachedFile = new File(cachedFilePath);
        File cachedFileValue = new File(cachedFilePath + ".value");

        boolean updateCache = false;

        if (!cachedFile.exists()) {
            updateCache = true;
        } else if (!cachedFileValue.exists()) {
            updateCache = true;
        } else {
            String previousValue = null;
            try {
                previousValue = FileUtils.readFileToString(cachedFileValue);
            } catch (IOException e) {
                updateCache = true;
            }
            if ((previousValue == null) || !previousValue.equals(value)) {
                updateCache = true;
            }
        }

        if (updateCache) {
            deleteCache(cachedFile, cachedFileValue);

            try {
                // Save value
                FileUtils.writeStringToFile(cachedFileValue, value);

                // Save content to cache
                FileOutputStream fos = new FileOutputStream(cachedFile);
                renderer.render(fos, value);
                fos.close();
            } catch (IOException e) {
                // Failed to write cache...
                deleteCache(cachedFile, cachedFileValue);
                throw new Exception("Failed to render file", e);
            }
        }
        return cachedFile;
    }

    private static void deleteCache(File cachedFile, File cachedFileValue) {
        if (cachedFile.exists()) {
            cachedFile.delete();
        }
        if (cachedFileValue.exists()) {
            cachedFileValue.delete();
        }
    }

    public static void clearCache() {
        try {
            File folder = new File(getFileCachePath());
            FileUtils.cleanDirectory(folder);
            LOG.warn("cache folder cleaned");
        } catch (IOException e) {
            LOG.warn("Cannot clean cache folder");
        }
    }

    private static String getCachedFilePath(String key) {
        String fileCacheFolder = getFileCachePath();
        String hexString = Integer.toHexString(key.hashCode());
        hexString = StringUtils.leftPad(hexString, PADDING_SIZE, '0');
        hexString = StringUtils.reverse(hexString);
        String filePath = fileCacheFolder + hexString.charAt(0) + File.separatorChar + hexString.charAt(1)
                + File.separatorChar + hexString.charAt(2) + "/" + hexString;
        return filePath;
    }

    private static String getFileCachePath() {
        return PreferenceService.getString("gazelle_home_path") + File.separatorChar + "fileCache"
                + File.separatorChar;
    }

    public static void updateFile(String key, String value, FileCacheRenderer renderer) throws Exception {
        forceCache(key, value, renderer);
    }
}
