package net.ihe.gazelle.common.servletfilter;

import net.ihe.gazelle.services.GenericServiceLoader;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.Filter;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.web.AbstractFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Scope(ScopeType.APPLICATION)
@Name("net.ihe.gazelle.common.servletfilter.SQLinjection")
@Install
@BypassInterceptors
@Filter
public class SQLinjectionFilter extends AbstractFilter {

    private static Logger log = LoggerFactory.getLogger(SQLinjectionFilter.class);

    private static String[] keyWords = {"/*", "*/", "'exec", "'select", "'update", "'delete", "'insert", "'alter",
            "'drop", "'create", "'shutdown", "\0"};

    private static Boolean sqlInjectionFilterEnabled;

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException,
            ServletException {

        HttpServletRequest originalRequest = (HttpServletRequest) req;

        if (sqlInjectionFilterEnabled == null) {
            final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
            if (createContexts) {
                Lifecycle.beginCall();
            }
            final CSPPoliciesPreferences applicationPreferenceManager = GenericServiceLoader
                    .getService(CSPPoliciesPreferences.class);
            if (applicationPreferenceManager != null) {
                sqlInjectionFilterEnabled = applicationPreferenceManager.getSqlInjectionFilterSwitch();
            }
            if (createContexts) {
                Lifecycle.endCall();
            }
        }
        if (sqlInjectionFilterEnabled != null && sqlInjectionFilterEnabled) {
            if (isUnsafe(originalRequest.getParameterMap())) {
                logSqlInjectionAttempt(originalRequest);
                req = new AntiSQLRequest(originalRequest);
            }
        }
        filterChain.doFilter(req, resp);
    }

    public static void resetInjectionFilter() {
        sqlInjectionFilterEnabled = null;
    }

    private void logSqlInjectionAttempt(HttpServletRequest originalRequest) {
        StringBuffer sb = new StringBuffer();
        sb.append("\nPossible SQL injection attempt at " + new java.util.Date());
        sb.append("\nRemote Address: " + originalRequest.getRemoteAddr());
        sb.append("\nSession Id: " + originalRequest.getRequestedSessionId());
        sb.append("\nURI: " + originalRequest.getContextPath() + originalRequest.getRequestURI());
        sb.append("\nParameters via " + originalRequest.getMethod());
        Map<String, String[]> paramMap = originalRequest.getParameterMap();
        for (Iterator<String> iter = paramMap.keySet().iterator(); iter.hasNext(); ) {
            String paramName = iter.next();
            String[] paramValues = originalRequest.getParameterValues(paramName);
            sb.append("\n\t" + paramName + " = ");
            for (int j = 0; j < paramValues.length; j++) {
                sb.append(paramValues[j]);
                if (j < (paramValues.length - 1)) {
                    sb.append(" , ");
                }
            }
        }
        log.warn(sb.toString());
    }

    @Override
    public void destroy() {
    }

    static boolean isUnsafe(Map<String, String[]> parameterMap) {
        Iterator<String> iter = parameterMap.keySet().iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            String[] param = parameterMap.get(key);
            for (int i = 0; i < param.length; i++) {
                if (isUnsafe(param[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    static boolean isUnsafe(String value) {
        String lowerCase = value.toLowerCase();
        for (int i = 0; i < keyWords.length; i++) {
            if (lowerCase.indexOf(keyWords[i]) >= 0) {
                log.warn("Escaped char: " + keyWords[i]);
                return true;
            }
        }
        return false;
    }

    static Map<String, String[]> getSafeParameterMap(Map parameterMap) {
        Map<String, String[]> newMap = new HashMap<String, String[]>();
        for (Object o : parameterMap.keySet()) {
            String key = (String) o;
            String[] oldValues = (String[]) parameterMap.get(key);
            String[] newValues = new String[oldValues.length];
            for (int i = 0; i < oldValues.length; i++) {
                newValues[i] = getSafeValue(oldValues[i]);
            }
            newMap.put(key, newValues);
        }
        return Collections.unmodifiableMap(newMap);
    }

    static String getSafeValue(String oldValue) {
        StringBuffer sb = new StringBuffer(oldValue);
        String lowerCase = oldValue.toLowerCase();
        for (int i = 0; i < keyWords.length; i++) {
            int x = -1;
            while ((x = lowerCase.indexOf(keyWords[i])) >= 0) {
                if (keyWords[i].length() == 1) {
                    sb.replace(x, x + 1, " ");
                    lowerCase = sb.toString().toLowerCase();
                    continue;
                }
                sb.deleteCharAt(x + 1);
                lowerCase = sb.toString().toLowerCase();
            }
        }
        return sb.toString();
    }
}
