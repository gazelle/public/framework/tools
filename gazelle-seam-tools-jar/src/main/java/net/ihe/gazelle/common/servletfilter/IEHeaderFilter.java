package net.ihe.gazelle.common.servletfilter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.Filter;
import org.jboss.seam.web.AbstractFilter;

@Scope(ScopeType.APPLICATION)
@Name("net.ihe.gazelle.common.servletfilter.IEHeaderFilter")
@Install
@BypassInterceptors
@Filter
public class IEHeaderFilter extends AbstractFilter {

	private static final String HEADER_NAME1 = "X-UA-Compatible";
	private static final String HEADER_VALUE1 = "IE=EmulateIE8,chrome=1";
	private static final String HEADER_NAME2 = "Cache-Control";
	private static final String HEADER_VALUE2 = "no-cache";

	public static final ThreadLocal<HttpServletRequest> REQUEST = new ThreadLocal<HttpServletRequest>();

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		if (response instanceof HttpServletResponse) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			REQUEST.set(httpServletRequest);
			if (httpServletRequest.getRequestURI().endsWith(".seam")) {
				if (!httpServletResponse.containsHeader(HEADER_NAME1)) {
					httpServletResponse.setHeader(HEADER_NAME1, HEADER_VALUE1);
				}
				if (!httpServletResponse.containsHeader(HEADER_NAME2)) {
					httpServletResponse.setHeader(HEADER_NAME2, HEADER_VALUE2);
				}
			}
		}
		chain.doFilter(request, response);
	}

}
