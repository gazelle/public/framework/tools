package net.ihe.gazelle.common.fineuploader;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;

public class RequestParser {
	private static String FILENAME_PARAM = "qqfile";

	private String filename;
	private FileItem uploadItem;

	private String id;
	private String beanName;

	private String param;

	private RequestParser() {
	}

	// 2nd param is null unless a MPFR
	static RequestParser getInstance(HttpServletRequest request, MultipartUploadParser multipartUploadParser)
			throws Exception {
		RequestParser requestParser = new RequestParser();

		if (multipartUploadParser != null) {
			requestParser.uploadItem = multipartUploadParser.getFirstFile();
			requestParser.filename = multipartUploadParser.getFirstFile().getName();
		} else {
			requestParser.filename = request.getParameter(FILENAME_PARAM);
			byte[] fileNameBytes = requestParser.filename.getBytes("ISO-8859-1");
			requestParser.filename = new String(fileNameBytes, "UTF-8");
		}

		requestParser.id = request.getParameter("id");
		requestParser.beanName = request.getParameter("beanName");
		requestParser.param = request.getParameter("param");

		return requestParser;
	}

	public String getFilename() {
		return filename;
	}

	// only non-null for MPFRs
	public FileItem getUploadItem() {
		return uploadItem;
	}

	public String getId() {
		return id;
	}

	public String getBeanName() {
		return beanName;
	}

	public String getParam() {
		return param;
	}

}