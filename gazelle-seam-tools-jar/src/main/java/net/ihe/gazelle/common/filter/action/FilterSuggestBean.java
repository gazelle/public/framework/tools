/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.filter.action;

import net.ihe.gazelle.common.LinkDataProvider;
import net.ihe.gazelle.common.LinkDataProviderService;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.hql.criterion.PropertyCriterion;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Name("filterSuggestBean")
@Scope(ScopeType.PAGE)
public class FilterSuggestBean implements Serializable {

    private static final long serialVersionUID = 6674342811351430146L;

    private static Logger log = LoggerFactory.getLogger(FilterSuggestBean.class);

    public String getSuggestionId(String fieldId, String idSuffix) {
        return fieldId + "Suggestion" + idSuffix;
    }

    public boolean supportsLink(Filter<?> filter, String filterId) {
        if ((filter != null) && (filter.getCriterions() != null)) {
            HQLCriterion<?, ?> criterion = filter.getCriterions().get(filterId);
            if (criterion != null) {
                Class<?> valueClass = criterion.getSelectableClass();
                if (criterion instanceof PropertyCriterion) {
                    PropertyCriterion propertyCriterion = (PropertyCriterion) criterion;
                    valueClass = propertyCriterion.getRealSelectableClass();
                }
                LinkDataProvider provider = LinkDataProviderService.getProviderForClass(valueClass);
                return provider != null;
            } else {
                log.error("Failed to find criterion in filter : " + filterId);
                return false;
            }
        } else {
            return false;
        }
    }
}
