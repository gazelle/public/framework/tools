
package net.ihe.gazelle.common.action;
import javax.ejb.Local;

@Local
public interface GenericConverterLocal {

	void registerConverters();

	void init();

	void stopGenericConverter();

}
