package net.ihe.gazelle.common.tree;

import org.richfaces.model.TreeNodeImpl;

import java.io.Serializable;

public class GazelleTreeNodeImpl<T> extends TreeNodeImpl implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private T data;
    private boolean expanded = true;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}