package net.ihe.gazelle.common.filter.list;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class SortDynamic<T> {

    private static Logger log = LoggerFactory.getLogger(SortDynamic.class);

    public List<T> sortList(List<T> listElements, final Map<String, Boolean> mapOrder) {
        Comparator<T> dynamicComparator = new Comparator<T>() {

            @Override
            public int compare(T o1, T o2) {
                if (o1 == null || o2 == null) {
                    return 0;
                }
                for (Map.Entry<String, Boolean> prop : mapOrder.entrySet()) {
                    String val1 = FieldProvider.getElementPropertyValueAsString(o1, prop.getKey());
                    String val2 = FieldProvider.getElementPropertyValueAsString(o2, prop.getKey());
                    if (val1 == null && val2 == null) {
                        continue;
                    }
                    if (val2 == null) {
                        return getComparisonPerOrder(1, mapOrder.get(prop.getKey()));
                    }
                    if (val1 == null) {
                        return getComparisonPerOrder(-1, mapOrder.get(prop.getKey()));
                    }
                    if (val1.equals(val2)) {
                        continue;
                    }

                    int compareRes = val1.compareTo(val2);
                    if (isInteger(val1) && isInteger(val2)) {
                        compareRes = Integer.valueOf(val1).compareTo(Integer.valueOf(val2));
                    }
                    return getComparisonPerOrder(compareRes, mapOrder.get(prop.getKey()));
                }
                return 0;
            }

            private boolean isInteger(String val1) {
                try {
                    Integer.valueOf(val1);
                } catch (Exception e) {
                    return false;
                }
                return true;
            }
        };
        Collections.sort(listElements, dynamicComparator);
        return listElements;
    }

    private int getComparisonPerOrder(int val, boolean order) {
        int valorder = 1;
        if (!order) {
            valorder = -1;
        }
        return val * valorder;
    }
}
