package net.ihe.gazelle.common.servletfilter;

import net.ihe.gazelle.common.util.GazelleCookie;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * This filter create a cookie to identify the browser.
 */
public class CookieBrowserFilter implements Filter {

    public static final String COOKIE_BROWSER_ID = "BROWSER_ID";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Nothing to do here
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {
            if (GazelleCookie.getCookie((HttpServletRequest) request, COOKIE_BROWSER_ID) == null) {
                GazelleCookie
                        .setCookie((HttpServletResponse) response, COOKIE_BROWSER_ID, UUID.randomUUID().toString());
            }
        }

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // Nothing to do here
    }
}
