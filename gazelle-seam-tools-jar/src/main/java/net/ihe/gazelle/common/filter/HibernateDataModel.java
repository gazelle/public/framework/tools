package net.ihe.gazelle.common.filter;

import net.ihe.gazelle.common.filter.list.FieldProvider;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import org.ajax4jsf.model.*;
import org.richfaces.component.SortOrder;
import org.richfaces.model.Arrangeable;
import org.richfaces.model.ArrangeableState;
import org.richfaces.model.FilterField;
import org.richfaces.model.SortField;

import javax.el.ELException;
import javax.el.Expression;
import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class HibernateDataModel<T> extends ExtendedDataModel<T>
        implements Arrangeable, Serializable {

    private static final long serialVersionUID = 8773031927238386576L;

    private Class<T> dataClass;

    private EntityManager entityManager;
    private Object dataItem;
    private SequenceRange cachedRange;
    private transient WeakReference<List<T>> weakCache;
    private ArrangeableState arrangeableState;

    public HibernateDataModel(Class dataClass) {
        super();
        this.dataClass = dataClass;
    }

    private static boolean areEqualRanges(SequenceRange range1,
                                          SequenceRange range2) {
        if ((range1 == null) || (range2 == null)) {
            return (range1 == null) && (range2 == null);
        } else {
            return (range1.getFirstRow() == range2.getFirstRow())
                    && (range1.getRows() == range2.getRows());
        }
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private String getPropertyName(FacesContext facesContext,
                                   Expression expression) {
        try {
            return (String) ((ValueExpression) expression)
                    .getValue(facesContext.getELContext());
        } catch (ELException e) {
            throw new FacesException(e.getMessage(), e);
        }
    }

    @Override
    public Object getRowKey() {
        return dataItem;
    }

    @Override
    public void setRowKey(Object key) {
        this.dataItem = key;
    }

    public List<T> getAllItems(FacesContext facesContext) {
        walk(facesContext, new DataVisitor() {
            @Override
            public DataVisitResult process(FacesContext facescontext,
                                           Object obj, Object obj1) {
                return DataVisitResult.CONTINUE;
            }
        }, null, null);
        return getWeakCache().get();
    }

    protected void appendFilters(FacesContext context,
                                 HQLQueryBuilder<T> queryBuilder) {
        //
    }

    protected void appendUserFilters(FacesContext context,
                                     HQLQueryBuilder<T> queryBuilder) {
        if (arrangeableState != null && (arrangeableState.getFilterFields() != null)
                && (context != null)) {
            for (FilterField filterField : arrangeableState
                    .getFilterFields()) {
                String filterValue = (String) filterField.getFilterValue();
                if ((filterValue != null) && (filterValue.length() != 0)) {
                    String propertyName = getPropertyName(context,
                            filterField.getFilterExpression());
                    if (propertyName != null) {
                        queryBuilder.addLike(propertyName, filterValue);
                    } else {
                        System.err.println(filterField
                                .getFilterExpression()
                                + " is not a valid value");
                    }
                }
            }
        }
    }

    protected void appendSorts(FacesContext context,
                               HQLQueryBuilder<T> queryBuilder) {
        if (arrangeableState != null && (arrangeableState.getSortFields() != null) && (context != null)) {
            for (SortField sortField : arrangeableState.getSortFields()) {
                SortOrder ordering = sortField.getSortOrder();

                if (SortOrder.ascending.equals(ordering)
                        || SortOrder.descending.equals(ordering)) {
                    String propertyName = getPropertyName(context,
                            sortField.getSortBy());
                    if (propertyName != null) {
                        queryBuilder.addOrder(propertyName,
                                SortOrder.ascending.equals(ordering));
                    } else {
                        System.err.println(sortField.getSortBy()
                                + " is not a valid sort");
                    }
                }
            }
        }
    }

    @Override
    public void walk(FacesContext facesContext, DataVisitor visitor,
                     Range range, Object argument) {

        SequenceRange jpaRange = (SequenceRange) range;

        List<T> result = getWeakCache().get();
        if ((result == null) || !areEqualRanges(this.cachedRange, jpaRange)) {

            HQLQueryBuilder<T> queryBuilder = prepareQueryBuilder(facesContext,
                    true);

            if (jpaRange != null) {
                int first = jpaRange.getFirstRow();
                int rows = jpaRange.getRows();

                queryBuilder.setFirstResult(first);
                if (rows > 0) {
                    queryBuilder.setMaxResults(rows);
                }
            }

            this.cachedRange = jpaRange;
            result = (List<T>) getRealCache(queryBuilder);
            weakCache = new WeakReference<List<T>>(result);
        }

        for (T t : result) {
            if (dataClass.isInstance(t)) {
                visitor.process(facesContext, getId(t), argument);
            } else {
                String id = FieldProvider.getElementPropertyValueAsString(t, "id");
                visitor.process(facesContext, t, argument);
            }
        }
    }

    public HQLQueryBuilder<T> prepareQueryBuilder(FacesContext facesContext,
                                                  boolean appendSorts) {
        HQLQueryBuilder<T> queryBuilder;
        if (getEntityManager() != null) {
            queryBuilder = new HQLQueryBuilder<T>(getEntityManager(), dataClass);
        } else {
            queryBuilder = new HQLQueryBuilder<T>(dataClass);
        }
        appendFilters(facesContext, queryBuilder);
        appendUserFilters(facesContext, queryBuilder);
        if (appendSorts) {
            appendSorts(facesContext, queryBuilder);
        }
        return queryBuilder;
    }

    protected int getRealCount(HQLQueryBuilder<T> queryBuilder) {
        return queryBuilder.getCount();
    }

    protected List<?> getRealCache(HQLQueryBuilder<T> queryBuilder) {
        return queryBuilder.getList();
    }

    /**
     * For list compatibility
     *
     * @return row count
     */
    public int size() {
        return getRowCount();
    }

    @Override
    public int getRowCount() {
        HQLQueryBuilder<T> queryBuilder = prepareQueryBuilder(
                FacesContext.getCurrentInstance(), false);
        return getRealCount(queryBuilder);
    }

    @Override
    public T getRowData() {
        if (this.dataItem != null && this.dataItem instanceof Integer) {
            HQLQueryBuilder<T> queryBuilder;
            if (getEntityManager() != null) {
                queryBuilder = new HQLQueryBuilder<T>(getEntityManager(), dataClass);
            } else {
                queryBuilder = new HQLQueryBuilder<T>(dataClass);
            }
            queryBuilder.addEq("id", this.dataItem);
            return queryBuilder.getUniqueResult();
        } else {
            return (T) this.dataItem;
        }
    }

    @Override
    public int getRowIndex() {
        return -1;
    }

    @Override
    public void setRowIndex(int rowIndex) {
    }

    @Override
    public Object getWrappedData() {
        return getWeakCache().get();
    }

    @Override
    public void setWrappedData(Object data) {
    }

    @Override
    public boolean isRowAvailable() {
        return (this.dataItem != null);
    }

    private void sortSortFields2(List<SortField> sortFields2) {
        Collections.sort(sortFields2, new Comparator<SortField>() {
            @Override
            public int compare(SortField o1, SortField o2) {
                String o1e = null;
                if (o1.getSortBy() != null) {
                    o1e = o1.getSortBy().getExpressionString();
                }
                String o2e = null;
                if (o2.getSortBy() != null) {
                    o2e = o2.getSortBy().getExpressionString();
                }
                if ((o1e == null) && (o2e == null)) {
                    return 0;
                }
                if (o1e == null) {
                    return 1;
                }
                if (o2e == null) {
                    return -1;
                }
                return o1e.compareTo(o2e);
            }
        });
    }

    public void resetCache() {
        this.weakCache = new WeakReference<List<T>>(null);
        this.cachedRange = null;
    }

    public WeakReference<List<T>> getWeakCache() {
        if (weakCache == null) {
            weakCache = new WeakReference<List<T>>(null);
        }
        return weakCache;
    }

    public void setWeakCache(WeakReference<List<T>> weakCache) {
        this.weakCache = weakCache;
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        in.defaultReadObject();
        resetCache();
    }

    @Override
    public void arrange(FacesContext context, ArrangeableState state) {
        arrangeableState = state;
        /*
         * boolean modified = false; if(arrangeableState!=null){
		 * List<FilterField> filterFields = state.getFilterFields();
		 * List<SortField> sortFields = state.getSortFields(); List<FilterField>
		 * thisFilterFields =arrangeableState.getFilterFields(); List<SortField>
		 * thisSortFields =arrangeableState.getSortFields(); if ((filterFields
		 * == null) || !filterFields.equals(thisFilterFields)) { modified =
		 * true; }
		 *
		 * if (!modified) { if ((thisSortFields == null) && (sortFields !=
		 * null)) { modified = true; } else if ((thisSortFields != null) &&
		 * (sortFields == null)) { modified = true; } else if ((thisSortFields
		 * != null) && (sortFields != null)) { if (thisSortFields.size() !=
		 * sortFields.size()) { modified = true; } else {
		 * sortSortFields2(thisSortFields); sortSortFields2(sortFields); for
		 * (int i = 0; i < sortFields.size(); i++) { if (!modified) { SortField
		 * sortFieldNew = sortFields.get(i); SortField sortFieldOrig =
		 * thisSortFields.get(i);
		 *
		 * int orderingNew = -1; int orderingOrig = -1;
		 *
		 * if (sortFieldNew.getSortOrder() != null) { orderingNew =
		 * sortFieldNew.getSortOrder().ordinal(); } if
		 * (sortFieldOrig.getSortOrder() != null) { orderingOrig =
		 * sortFieldOrig.getSortOrder().ordinal(); }
		 *
		 * String expressionNew = null; String expressionOrig = null;
		 *
		 * if (sortFieldNew.getSortBy() != null) { expressionNew =
		 * sortFieldNew.getSortBy().getExpressionString(); } if
		 * (sortFieldOrig.getSortBy() != null) { expressionOrig =
		 * sortFieldOrig.getSortBy().getExpressionString(); }
		 *
		 * EqualsBuilder equalsBuilder = new EqualsBuilder(); if
		 * (!equalsBuilder.append(orderingNew,
		 * orderingOrig).append(expressionNew, expressionOrig) .isEquals()) {
		 * modified = true; } } } } } }
		 *
		 * if (modified) { this.arrangeableState = state; resetCache(); }
		 *
		 * }
		 */
        resetCache();
    }

    protected abstract Object getId(T t);

    public Class<T> getDataClass() {
        return dataClass;
    }

    public void setDataClass(Class<T> dataClass) {
        this.dataClass = dataClass;
    }

    public Object getDataItem() {
        return dataItem;
    }

    public void setDataItem(Object dataItem) {
        this.dataItem = dataItem;
    }

    public SequenceRange getCachedRange() {
        return cachedRange;
    }

    public void setCachedRange(SequenceRange cachedRange) {
        this.cachedRange = cachedRange;
    }

    public ArrangeableState getArrangeableState() {
        return arrangeableState;
    }

    public void setArrangeableState(ArrangeableState arrangeableState) {
        this.arrangeableState = arrangeableState;
    }
}
