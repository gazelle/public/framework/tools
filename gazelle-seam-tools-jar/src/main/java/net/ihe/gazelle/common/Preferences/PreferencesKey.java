package net.ihe.gazelle.common.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum PreferencesKey {
    SECURITY_POLICIES("security-policies", "java.lang.Boolean", "false", "Enable or Disable http security headers"),
    X_FRAME_OPTIONS("X-Frame-Options", Constants.JAVA_LANG_STRING, "SAMEORIGIN", "Sites can use this to avoid clickjacking attacks, by ensuring that their content is not embedded into other sites"),
    CACHE_CONTROL("Cache-Control", Constants.JAVA_LANG_STRING, "private, no-cache, no-store, must-revalidate, max-age=0", "Application should return caching directives instructing browsers not to store local copies of any sensitive data."),
    STRICT_TRANSPORT_SECURITY("Strict-Transport-Security", Constants.JAVA_LANG_STRING, "max-age=31536000 ; includeSubDomains", "is a security feature that lets a web site tell browsers that it should only be communicated with using HTTPS, instead of using HTTP"),
    SECURITY_CONTENT_SECURITY_POLICIES("X-Content-Security-Policy", Constants.JAVA_LANG_STRING, "", " is an added layer of security that helps to detect and mitigate certain types of attacks, including Cross Site Scripting (XSS) and data injection attacks"),
    SECURITY_CONTENT_SECURITY_POLICIES_CHROME("X-WebKit-CSP", Constants.JAVA_LANG_STRING, "Use X-Content-Security-Policy values", "Chrome flag, uses X-Content-Security-Policy values"),
    SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY_CHROME("X-WebKit-CSP-Report-Only", Constants.JAVA_LANG_STRING, "Use X-Content-Security-Policy-Report-Only values", "Chrome flag, uses X-Content-Security-Policy-Report-Only values"),
    SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY("X-Content-Security-Policy-Report-Only", Constants.JAVA_LANG_STRING, "default-src 'self' *.ihe.net www.epsos.eu; script-src 'self' 'unsafe-eval' 'unsafe-inline'; style-src 'self' 'unsafe-inline';", "Configuring Content Security Policy involves deciding what policies you want to enforce, and then configuring them and using X-Content-Security-Policy to establish your policy."),
    SQL_INJECTION_FILTER_SWITCH("sql_injection_filter_switch", "java.lang.Boolean", "false", "Enable or Disable Sql Injection filter");

    private String friendlyName;
    private String type;
    private String defaultValue;
    private String description;

    private static Logger log = LoggerFactory.getLogger(PreferencesKey.class);

    PreferencesKey(String friendlyName, String type, String defaultValue, String description) {
        this.friendlyName = friendlyName;
        this.type = type;
        this.defaultValue = defaultValue;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getType() {
        return type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public static PreferencesKey valueOfByFriendlyName(String name) {
        for (PreferencesKey value : values()) {
            String preferenceName = value.getFriendlyName();
            if (preferenceName.equals(name)) {
                return value;
            }
        }

        return null;
    }

    private static class Constants {
        public static final String JAVA_LANG_STRING = "java.lang.String";
    }
}
