package net.ihe.gazelle.common.action;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.HashMap;
import java.util.Map;

@Name("cacheRequest")
@Scope(ScopeType.EVENT)
public class CacheRequest extends CacheAbstract {
    private Map<String, Object> cache;

    @Override
    public Object getValue(String key) {
        return cache.get(key);
    }

    @Override
    public void setValue(String key, Object value) {
        cache.put(key, value);
    }

    @Override
    public void removeValue(String key) {
        cache.remove(key);
    }

    @Create
    public void init() {
        cache = new HashMap<String, Object>();
    }

    @Destroy
    public void clear() {
        if (cache != null) {
            cache.clear();
        }
    }
}
