package net.ihe.gazelle.common.pages;

public class AuthorizationOr implements Authorization {

	private Authorization[] authorizations;

	public AuthorizationOr(Authorization... authorizations) {
		super();
		this.authorizations = authorizations;
	}

	@Override
	public boolean isGranted(Object... context) {
		for (Authorization authorization : authorizations) {
			if (authorization.isGranted(context)) {
				return true;
			}
		}
		return false;
	}

}
