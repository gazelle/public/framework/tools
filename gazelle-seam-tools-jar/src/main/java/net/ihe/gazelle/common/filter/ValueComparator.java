package net.ihe.gazelle.common.filter;

import java.util.Comparator;

import net.ihe.gazelle.hql.NullValue;
import net.ihe.gazelle.hql.criterion.HQLCriterion;

public class ValueComparator implements Comparator<Object> {

	private HQLCriterion<?, Object> criterion;

	public ValueComparator(HQLCriterion<?, Object> criterion) {
		super();
		this.criterion = criterion;
	}

	@Override
	public int compare(Object o1, Object o2) {
		if ((o1 == null) || NullValue.NULL_VALUE.equals(o1)) {
			return -1;
		}
		if ((o2 == null) || NullValue.NULL_VALUE.equals(o2)) {
			return 1;
		}
		String l1 = criterion.getSelectableLabel(o1);
		if (l1 == null) {
			return -1;
		}
		String l2 = criterion.getSelectableLabel(o2);
		if (l2 == null) {
			return 1;
		}
		return l1.compareToIgnoreCase(l2);
	}

}
