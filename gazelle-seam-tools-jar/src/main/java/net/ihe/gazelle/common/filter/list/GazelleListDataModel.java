package net.ihe.gazelle.common.filter.list;

import net.ihe.gazelle.common.filter.NoHQLQueryBuilder;

import java.util.List;

public class GazelleListDataModel<T> extends NoHQLDataModel<T> {

   private static final long serialVersionUID = 1L;

   private final List<T> data;

   public GazelleListDataModel(List<T> data) {
      this(data, null);
   }

   public GazelleListDataModel(List<T> data,  String dataTableId) {
      super(dataTableId);
      this.data = data;
   }

   @Override
   public NoHQLQueryBuilder<T> newQueryBuilder() {
      return new ListQueryBuilder<>(data);
   }
}
