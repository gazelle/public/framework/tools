package net.ihe.gazelle.common.filter;

import net.ihe.gazelle.common.filter.action.DatatableStateHolderBean;
import net.ihe.gazelle.common.filter.util.MapNotifier;
import net.ihe.gazelle.common.util.DataScrollerMemory;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.richfaces.component.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class UrlFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(UrlFilter.class);

    private static final String SORT = "sort.";
    private static final String FILTER = "filter.";
    private static final String PAGE = "page.";
    private static final String ROWS = "rows";

    public MapNotifier<String, String> initFromUrl(Map<String, String> requestParameterMap) {
        MapNotifier<String, String> filterValues = new MapNotifier<>(new HashMap<String, String>());
        DatatableStateHolderBean dataTableState = (DatatableStateHolderBean) Component.getInstance("dataTableStateHolder");
        DataScrollerMemory scrollerMemory = (DataScrollerMemory) Component.getInstance("dataScrollerMemory");

        for (Map.Entry<String, String> entry : requestParameterMap.entrySet()) {
            String param = entry.getKey();
            String value = fixEncoding(entry.getValue());

            if (param.startsWith(FILTER) && dataTableState != null) {
                param = StringUtils.replaceOnce(param, FILTER, "");
                dataTableState.getColumnFilterValues().put(param, value);
            } else if (param.startsWith(SORT) && dataTableState != null) {
                param = StringUtils.replaceOnce(param, SORT, "");
                SortOrder orderingValue = SortOrder.valueOf(value);
                dataTableState.getSortOrders().put(param, orderingValue);
            } else if(param.startsWith(PAGE) && scrollerMemory != null) {
                param = StringUtils.replaceOnce(param, PAGE, "");
                scrollerMemory.getScrollerPage().put(param, Integer.valueOf(value));
            } else if(param.startsWith(ROWS) && scrollerMemory != null) {
                scrollerMemory.setNumberOfResultsPerPage(Integer.valueOf(value));
            } else {
                if (value.equals("null")) {
                    filterValues.put(param, null);
                } else {
                    filterValues.put(param, value);
                }
            }
        }
        return filterValues;
    }

    public String getUrlParameters(Map<String, String> filterValues) {
        StringBuilder urlStringBuilder = new StringBuilder();
        addFilterParams(urlStringBuilder, filterValues);

        DatatableStateHolderBean dataTableState = (DatatableStateHolderBean) Component.getInstance("dataTableStateHolder");
        addTableStateFilterParams(urlStringBuilder, dataTableState);
        addSortParams(urlStringBuilder, dataTableState);

        DataScrollerMemory scrollerMemory = (DataScrollerMemory) Component.getInstance("dataScrollerMemory");
        addPageParams(urlStringBuilder, scrollerMemory);

        return urlStringBuilder.toString();
    }

    private static void addFilterParams(StringBuilder urlString, Map<String, String> filterValues) {
        for (Map.Entry<String, String> filter : filterValues.entrySet()) {
            if (filter.getValue() != null) {
                try {
                    String valueEncoded = URLEncoder.encode(filter.getValue(), StandardCharsets.UTF_8.toString());
                    appendParam(urlString, null, filter.getKey(), valueEncoded);
                } catch (UnsupportedEncodingException e) {
                    // Do not append the parameter
                }
            }
        }
    }

    private static void addTableStateFilterParams(StringBuilder urlString, DatatableStateHolderBean dataTableState) {
        for (Map.Entry<String, Object> entry : dataTableState.getColumnFilterValues().entrySet()) {
            if (entry.getValue() != null) {
                appendParam(urlString, FILTER, entry.getKey(), entry.getValue());
            }
        }
    }

    private void addPageParams(StringBuilder urlString, DataScrollerMemory scrollerMemory) {
        for(Map.Entry<String, Integer> pageParam : scrollerMemory.getScrollerPage().entrySet()) {
            if(pageParam.getValue() != null) {
                appendParam(urlString, PAGE, pageParam.getKey(), pageParam.getValue());
            }
        }
        appendParam(urlString, null, ROWS, scrollerMemory.getNumberOfResultsPerPage());
    }

    private static void addSortParams(StringBuilder urlString, DatatableStateHolderBean dataTableState) {
        Map<String, SortOrder> sortOrders = dataTableState.getSortOrders();
        Set<Map.Entry<String, SortOrder>> sortEntrySet = sortOrders.entrySet();
        for (Map.Entry<String, SortOrder> entry : sortEntrySet) {
            if (entry.getValue() != SortOrder.unsorted) {
                appendParam(urlString, SORT, entry.getKey(), entry.getValue());
            }
        }
    }

    private static void appendParam(StringBuilder builder, String paramPrefix, String param, Object value) {
        if (builder.length() != 0) {
            builder.append('&');
        }
        if(paramPrefix != null) {
            builder.append(paramPrefix);
        }
        builder.append(param).append('=').append(value);
    }

    private static String fixEncoding(String value) {
        // Convert the input string to bytes using UTF-8 encoding
        byte[] bytes = value.getBytes(StandardCharsets.ISO_8859_1);
        // Convert the bytes back to a string using UTF-8 encoding
        return new String(bytes, StandardCharsets.UTF_8);
    }
}
