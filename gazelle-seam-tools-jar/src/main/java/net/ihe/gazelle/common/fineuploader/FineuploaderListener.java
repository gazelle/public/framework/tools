package net.ihe.gazelle.common.fineuploader;

import java.io.File;
import java.io.IOException;

@javax.ejb.Local
public interface FineuploaderListener {

	void uploadedFile(File tmpFile, String filename, String id, String param) throws IOException;

}
