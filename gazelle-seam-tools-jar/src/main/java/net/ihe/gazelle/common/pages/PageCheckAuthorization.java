package net.ihe.gazelle.common.pages;

public class PageCheckAuthorization {

	private PageCheckAuthorization() {
		super();
	}

	public static boolean isGranted(Page page, Object... context) {
		Authorization[] authorizations = page.getAuthorizations();
		return new AuthorizationAnd(authorizations).isGranted(context);
	}

}
