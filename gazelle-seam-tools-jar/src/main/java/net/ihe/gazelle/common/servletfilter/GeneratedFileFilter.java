package net.ihe.gazelle.common.servletfilter;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.ihe.gazelle.services.GenericServiceLoader;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.Filter;
import org.jboss.seam.servlet.ContextualHttpServletRequest;
import org.jboss.seam.web.AbstractFilter;

@Scope(ScopeType.APPLICATION)
@Name("net.ihe.gazelle.common.servletfilter.GeneratedFileFilter")
@Install
@BypassInterceptors
@Filter
public class GeneratedFileFilter extends AbstractFilter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		boolean handled = false;
		if (response instanceof HttpServletResponse) {
			final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			final HttpServletResponse httpServletResponse = (HttpServletResponse) response;

			String servletPath = httpServletRequest.getServletPath();

			List<FileGenerator> providers = GenericServiceLoader.getServices(FileGenerator.class);
			for (final FileGenerator fileGenerator : providers) {
				if (servletPath.startsWith(fileGenerator.getPath())) {
					new ContextualHttpServletRequest(httpServletRequest) {
						@Override
						public void process() throws Exception {
							fileGenerator.process(httpServletRequest, httpServletResponse);
						}
					}.run();
					handled = true;
				}
			}

		}
		if (!handled) {
			chain.doFilter(request, response);
		}
	}

}
