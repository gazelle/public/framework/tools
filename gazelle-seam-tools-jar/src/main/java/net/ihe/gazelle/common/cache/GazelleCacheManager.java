package net.ihe.gazelle.common.cache;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.commons.lang.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author jlabbe
 */
public final class GazelleCacheManager {

    private static Logger log = LoggerFactory.getLogger(GazelleCacheManager.class);

    public static final int CONST_24_HOURS_IN_SECONDS = 86400;
    public static final int CONST_16_HOURS_IN_SECONDS = 43200;
    public static final int CONST_8_HOURS_IN_SECONDS = 21600;
    public static final int CONST_4_HOURS_IN_SECONDS = 10800;
    public static final int CONST_2_HOURS_IN_SECONDS = 5400;
    public static final int CONST_1_HOURS_IN_SECONDS = 2700;
    public static final int CONST_30_MINUTES_IN_SECONDS = 1350;

    private GazelleCacheManager() {
    }

    /**
     * Method queryWithCache. Perform the request using cache enhancements. It can return the requested value by performing a method call or performing a cache request
     * <p>
     * <pre>
     * Behavior If the Value is not found in the cache or if queryCache parameter is "off" then it performs the request and stores the result in the cache <!--  -->
     * If the Value is found in then the value is returned
     * </pre>
     *
     * @param request        GazelleCacheRequest
     * @param objectId       String
     * @param cacheKeyPrefix String
     * @param request_url    String
     * @param queryCache     String
     *
     * @return String
     */
    public static <T> T getWithCache(GazelleCacheRequest<T> request, String cacheKeyPrefix, String objectId,
                                     int time_to_live_in_seconds) {
        return GazelleCacheManager.getWithCache(request, cacheKeyPrefix, objectId, "", time_to_live_in_seconds);
    }

    private static <T> void cacheElement(T result, final Ehcache cache, final String cacheId,
                                         final int time_to_live_in_seconds) {
        if (result != null) {
            if ((result instanceof List)) {
                if (!((List) result).isEmpty()) {
                    cacheElement_(result, cache, cacheId, time_to_live_in_seconds);
                }
            } else {
                cacheElement_(result, cache, cacheId, time_to_live_in_seconds);
            }
        }
    }

    private static <T> void cacheElement_(T result, final Ehcache cache, final String cacheId,
                                          final int time_to_live_in_seconds) {
        final Element elm = new Element(cacheId, result);
        elm.setTimeToLive(time_to_live_in_seconds);
        cache.put(elm);
    }

    public static void clearElement(String cacheKeyPrefix, String objectId) {
        final Ehcache cache = CacheManager.getInstance().getEhcache("instances");
        if (cache != null) {
            final String cacheId = cacheId(cacheKeyPrefix, objectId);
            cache.remove(cacheId);
        }
    }

    /**
     * Method cacheId. Returns an id build with the given infos. Ids must be unique, so here we have a concatenation of: objectType:request_url:objectId separated by dash.
     *
     * @param request_url String
     * @param objectType  String
     * @param objectId    String
     *
     * @return String
     */
    private static String cacheId(String cacheKeyPrefix, String objectId) {
        return cacheKeyPrefix + ":" + objectId;
    }

    @SuppressWarnings("unchecked")
    private static <T> T getWithCache(GazelleCacheRequest<T> request, String cacheKeyPrefix, String objectId,
                                      String queryCache, int time_to_live_in_seconds) {
        T result = null;
        final Ehcache cache = CacheManager.getInstance().getEhcache("instances");
        final String cacheId = cacheId(cacheKeyPrefix, objectId);
        if (cache != null) {
            Element element = cache.get(cacheId);
            if ((null == element) || queryCache.equals("off")) {

                result = request.get(objectId);

                cacheElement(result, cache, cacheId, time_to_live_in_seconds);
            } else {
                result = (T) element.getObjectValue();
            }
        } else {
            result = request.get(objectId);
        }

        T object = (T) ObjectUtils.clone(result);
        if (object == null) {
            object = result;
        }
        return object;
    }
}
