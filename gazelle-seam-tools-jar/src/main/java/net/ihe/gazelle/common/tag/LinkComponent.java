package net.ihe.gazelle.common.tag;

import net.ihe.gazelle.common.LinkDataProvider;
import org.apache.commons.lang.StringUtils;

import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;

public class LinkComponent extends AbstractLinkComponent {

    public LinkComponent() {
        super();
    }

    @Override
    public String getFamily() {
        return "gazelle-link";
    }

    @Override
    public void outputLink(FacesContext context, ResponseWriter writer, Object value, boolean isDetailed,
                           String contextPath) throws IOException {
        LinkDataProvider provider = getProvider(value);

        boolean tooltip = getAttributeValueBoolean("tooltip");
        String tooltipString = null;

        if (tooltip) {
            writer.startElement("span", this);
            tooltipString = getTooltip(provider, value);
            if (StringUtils.trimToNull(tooltipString) == null) {
                tooltip = false;
            }
        }

        if (tooltip) {
            writer.writeAttribute("data-tooltip", tooltipString, null);
        }

        writer.startElement("a", this);
        String url = getURL(provider, value, contextPath);
        writer.writeURIAttribute("href", context.getExternalContext().encodeResourceURL(url), "href");

        String target = getAttributeValueString("target");
        if ((target != null) && (target.trim().length() != 0)) {
            writer.writeAttribute("target", target, "target");
        }
        String styleClass = getAttributeValueString("styleClass");
        if (styleClass != null) {
            writer.writeAttribute("class", styleClass, "styleClass");
        }
        String text = getText(provider, value, isDetailed);
        writer.writeText(text.toCharArray(), 0, text.length());
        writer.endElement("a");

        if (tooltip) {
            writer.endElement("span");
        }
    }
}
