# Gazelle Seam Tools JAR

<!-- TOC -->
* [Gazelle Seam Tools JAR](#gazelle-seam-tools-jar)
  * [Authorizations](#authorizations)
  * [Menu and pages](#menu-and-pages)
    * [Build the top-menu](#build-the-top-menu)
    * [Pages access control](#pages-access-control)
  * [Authentication and timeout error handling](#authentication-and-timeout-error-handling)
<!-- TOC -->

To use any of the feature listed, add maven dependency:

```xml
   <dependency>
       <groupId>net.ihe.gazelle.maven</groupId>
       <artifactId>gazelle-seam-tools-jar</artifactId>
       <type>ejb</type>
   </dependency>
```

## Authorizations

Gazelle-tools provides an authorization framework that allow performing boolean operation (AND, OR,
NOT) on sets and compositions of authorizations for page access.

See :

* `net.ihe.gazelle.common.pages.Authorization`
* `net.ihe.gazelle.common.pages.AuthorizationAnd`
* `net.ihe.gazelle.common.pages.AuthorizationNot`
* `net.ihe.gazelle.common.pages.AuthorizationOr`

There is an example in __gazelle-tm-ejb__ module. However, unlike __gazelle-tm-ejb__ implementation
that is making static calls to the user's session attributes, it is strongly advised to use
the `context` varargs parameter of the methods `Authorization#isGranted(Object... context)` to give
contextual information and target to test the authorization. That will greatly improve testability.

Those Authorizations can be also be consummed by Seam and `GazelleIdentity` providing an
implementation of permission resolver `org.jboss.seam.security.permission.PermissionResolver`.

This would permit to perform access control in a programmatic way:

```java
import org.jboss.seam.annotations.In;

public class MyService {
   @In
   private GazelleIdentity identity;

   @In
   private UserSession currentSession;

   public void updateSystem(System system) {
      if (identity.hasPermission("system", "update", currentSession.getTestingSession())) {
         //....
      }
   }
}
```

`GazelleIdentity` can be found in module __sso-client-api__.

## Menu and pages

Gazelle-tools provide menu UI components for Gazelle applications.

Every page/view of application should implement the interface `net.ihe.gazelle.common.pages.Page` to
reference its link, potential icon to display but also the required
[`Authorization`](#authorizations) to access it.

Those Pages can then be used for 2 purposes : [Build the top menu bar](#build-the-top-menu), but
also performing [access control based on requested URL](#pages-access-control).

### Build the top-menu

The menu is represented by the interface `net.ihe.gazelle.common.pages.menu.Menu` that is
implemented by the recursive structure of `MenuGroup` and `MenuEntry`.
It is technically possible to implement an infinite depth in the menu-tree, but the associated UI
component can only support a depth of 3 level, and for UX it is best to never go behond 2.

Every "final" `MenuEntry` (leaf) in the menu-tree must refer to a `Page`.

```java

@Named("myMenu")
public class MyMenu {

   public MenuGroup getMenu() {
      ArrayList<Menu> topMenu = new ArrayList<Menu>();
      topMenu.add(new MenuEntry(Pages.SINGLE_REGISTRATION));
      topMenu.add(getTestSession());
      topMenu.add(getPreparation());
      topMenu.add(getTesting());
      topMenu.add(new MenuEntry(Pages.CAT_RESULTS));
      topMenu.add(getManageRegistry());
      topMenu.add(getAdministration());
      return new MenuGroup(null, topMenu);
   }

   private Menu getTestSession() {
      List<Menu> testSessionMeny = new ArrayList<Menu>();
      testSessionMeny.add(new MenuEntry(Pages.REGISTRATION_PARTICIPANTS));
      testSessionMeny.add(new MenuEntry(Pages.REGISTRATION_SYSTEMS));
      testSessionMeny.add(new MenuEntry(Pages.REGISTRATION_OVERVIEW));
      testSessionMeny.add(new MenuEntry(Pages.ADMIN_MANAGE_MONITORS));
      return new MenuGroup(Pages.TEST_SESSION_MENU_GROUP, testSessionMeny);
   }

   //... other sub-menus

}
```

Then, call your menu builder from your re-definition of the __gazelle-assets__ UI component
`/layout/menu/_menu_template.xhtml`.

```xhtml

<ui:decorate template="/layout/menu/_menu_template.xhtml">

    <ui:define name="menu_title">
        <!-- ... -->
    </ui:define>

    <ui:define name="menu_logo">
        <!-- ... -->
    </ui:define>

    <ui:define name="menu_navigation_links">
        <ui:include src="/layout/menu/_generate_navigation_links.xhtml">
            <ui:param name="icon_template" value="/layout/menu/_fontawesome_icon_template.xhtml"/>
            <ui:param name="url_base_name" value="#{applicationManagerBean.getUrlBaseResource()}"/>
            <ui:param name="menu" value="#{myMenu.getMenu()}"/>
        </ui:include>
    </ui:define>

    <ui:define name="menu_user_actions_right">
        <!-- ... -->
    </ui:define>

</ui:decorate>
```

The component `/layout/menu/_generate_navigation_links.xhtml` will check the `Authorization`s
defined by all `Page`s of the menu to display the entry.

### Pages access control

`net.ihe.gazelle.common.pages.GazelleSecurityCheck` will ensure that the user is authorized to
request any view declared as `Page`.

To set that up, all the pages of an application must be referenced by an __SPI implementation__ of
`net.ihe.gazelle.common.pages.PageLister`.

```java

@MetaInfServices(PageLister.class)
public class GazellePageLister implements PageLister {

   @Override
   public Collection<Page> getPages() {
      return Arrays.asList(Pages.values());
      // Pages being an enumeration of all `Page`s of your application.
   }

}
```

Any view accessed without being declared in the `PageLister` will raise warnings in JBoss logs.

Finally, add the security description in the Seam descriptor file `WEB-INF/pages.xml` :

```xml
<?xml version="1.0" encoding="UTF-8"?>
<pages xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns="http://jboss.org/schema/seam/pages"
       xsi:schemaLocation="http://jboss.org/schema/seam/pages http://jboss.org/schema/seam/pages-2.3.xsd">

    <page view-id="*">
        <restrict>#{gazelleSecurityCheck.checkSecurity()}</restrict>
    </page>

    <!-- .. -->
</pages>
```

## Authentication and timeout error handling

`net.ihe.gazelle.common.pages.AuthzExceptionHandler` is an implementation of Seam ExceptionHandler
to redirect the user to the right path in case of:

1. `org.jboss.seam.security.AuthorizationException`, the user will be redirected to the home page
   with a message about unsufficient rights.
2. `org.jboss.seam.security.NotLoggedInException`, the user will be redirected to the login flow
   configured for your application using __sso-client-v7__ module.
3. `javax.faces.application.ViewExpiredException`, the user will be redirected to a restoration of
   the expired view if still logged-in.

This error handler requires to setup
the [sso-client-v7](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7.) dependency.

It also requires the CookieBrowserFilter to be setup in the `/WEB-INF/web.xml`:

```xml
    <filter>
        <filter-name>CookieBrowserFilter</filter-name>
        <filter-class>net.ihe.gazelle.common.servletfilter.CookieBrowserFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CookieBrowserFilter</filter-name>
        <url-pattern>*.seam</url-pattern>
    </filter-mapping>
```

